## Sharing is Caring
<div class="_202x">
    <span class="year">2022</span>
    <hr class="hr" />
</div>

<span class="first-words">Marlene und Fred</span> in einem Zimmer im Dachgeschoss von Hans' Holzhaus.
Auf dem Boden und dem Schreibtisch standen vier Laptops.
Beide saßen und tippten.

Fred: „Gutes Internet hier.
Wie verschleierst du, dass du Tor benutzt?
Es könnte schon auffallen, wenn jemand in der Mitte von Nirgendwo plötzlich jede Menge Tor-Daten ins Internet schickt, und Australien ist ja nicht gerade ein privatsphären-freundliches Land.“

Marlene: „Ganz normal: über Tor Pluggable Transports.
Ich bin gerade dabei, mir einen eigenen Eingang zu bauen.
Bisher nehme ich meistens die Web- oder die Skype-Option.
Teenager im Ausland surfen und skypen ja jede Menge.“

<div class="infobox" id="pluggabletransports"></div>

Fred nickte mit einem leisten Lächeln und tippte etwas auf seinem Laptop.

Fred: „Okay!
Daten sind fertig.
Ich bin soweit.
Ich kann jetzt nachschlagen, welche Schwachstelle es für welchen Computer oder welches Betriebssystem gibt.
Wer kommt zuerst dran?“

Marlene: „Wie gesagt: Tor, Tails, Cubes OS, GPG, die OMEMO-Projekte und VeraCrypt.“

<div class="infobox" id="gpg-omemo"></div>

„Wir schicken die Schwachstellen von allen ihren Projekten an sie.
Ich habe die Kernleute schon vorgewarnt: 
Da kommt ein Erdbeben, und eine Sintflut.
Die wissen genau was passiert, wenn die Informationen jetzt gleich an die falschen Leute gehen.
Und sie wissen auch wie man das verhindert.“

Fred: „Schicken wir alles?“

Marlene nickte: „Ja klar, alles über ihre Projekte.
Alles aus der Datenbank, die du gemacht hast.
Das ist wichtig, dass in den Programmen keine Lücken mehr sind, bevor die Sache richtig groß wird.
Wir müssen ja kommunizieren können.“

Fred: „Wie viel Zeit geben wir ihnen?“

Marlene: „Das sollen sie selbst sagen.
Drei Tage, schätze ich.“

Fred: „DREI TAGE?!
Bist du verrückt?
Wie können sie die Lücken in drei Tagen stopfen?“

Marlene: „Bei den sechs Projekten sind es nicht so viele.
Sie wissen genau, wonach sie schauen müssen.
Sie reagieren normalerweise bei Sicherheitslücken sofort, die sind hochaktiv in ihren Projekten.
Aber wir fragen sie.
Sie werden nicht viel mehr als drei Tage sagen.
Ich schreibe noch, dass wir unter Zeitdruck sind.“

Fred: „Und danach?
Ich habe hier jeweils ein Info-Paket für verschiedene Linux-Varianten, für BSD, eines für Mozilla, vor allem Firefox und Thunderbird, KeePassX und noch ein paar andere.“

<div class="infobox" id="thunderbird-etc"></div>

Marlene: „Ja, das klingt gut.
Ich habe inzwischen jede Menge aktuelle Ricochet-Adressen von allen möglichen Teams.
Die schicken wir sobald die Schwachstellen in den Basisprogrammen weg sind und wir alle die neuen Versionen haben.
Diese Teams brauchen mehr Zeit.
Das ist eine Menge Zeug.
Und dann lass uns alle anderen Linuxe, Libre Office und die Hardware-Dinge angehen.“

Fred: „Jede Menge Arbeit.“

Marlene: „Ja.“

Sie tippe auf ihrem Laptop.
„Okay ... so ... die Nachrichten an die ersten sechs Teams sind raus.
Ich denke, wir werden schnell Antwort bekommen.
Jetzt erzähl doch mal, wie war das mit Anita und dem Terabyte?“

Fred: „Ja!
Anita und das Terabyte.
Das ist eine Geschichte.
Also, nach dem Treffen mit allen, wo du im Boatly Hackerspace warst:
Ich bekomme deine Pond-Nachricht, die du auch Anita geschickt hast, und falle erst einmal vom Stuhl.
Ich hatte das mit dem Raspberry Pi im Chat nicht so ganz ernst gemeint: eher so ... könnte man machen.
Dann raffe ich mich wieder auf, atme durch, setze mich aufrecht hin und schreibe das Raspberry-Programm.
Nicht so schwierig.
Dreiviertelstunde.
Dann teste ich es mit ein paar tausend QR-Codes, ist okay, mache ein Image davon und schicke es über Pond an Anita.
Keine Antwort, nur ein normales Pond-Acknowledge.
Sie hat es bekommen.
Gut.
Als nächstes bekomme ich 22 Stunden 10 Minuten später eine Nachricht, dass die Dateien wohlbehalten auf zwei 1TB-Sticks liegen, mit der Frage, wie sie geliefert werden sollen.
Ich falle zum zweiten Mal von meinem Stuhl, krieche wieder hervor, setze mich wieder aufrecht hin und lese weiter.
Sie schreibt, sie wird vielleicht von NSA-Leuten verfolgt werden.
Sie ist nicht sicher, aber wenn sie irgendeinen Hinweis haben, dann wird sie überwacht.
Ihr Vorschlag ist, dass sie einen Stick in einer Raststätte der Toilettenfrau dort gibt, die da seit bestimmt 10, 15 Jahren sitzt.
Sie gibt ihr 50 Dollar und sagt, dass ein Mann zu ihr nach Hause kommen wird, der ihr für den Stick weitere 50 Dollar geben wird.
Sie hat mir die Hausadresse der Frau gegeben und gesagt, dass sie meistens um 18:20 Uhr zu Hause eintrifft.
Na ja, Anita muss das wissen.
Sie ist ja bei der NSA ...“

Marlene: „Shit.
Hat uns jetzt bei der Aktion die NSA-Überwachung geholfen?“

Fred: „Egal ...
Also ich fahre dort um halb sieben hin.
Sie öffnet die Tür schaut mich von oben bis unten an, dreht sich um und holt den Stick.
Ich strecke ihr 50 Dollar entgegen und sie sagt: 100 Dollar!
Ich schließe die Augen, warte zwei Sekunden und hole dann einen weiteren 50 Dollar-Schein aus der Tasche.
Sie macht bei alldem ein Gesicht, als ob sie gerade zum Spaß eine Gruppe von jungen Pudel ersäuft hätte.
Sie nimmt die zwei Scheine und gibt mir den Stick.
Ich habe ihn in der Hand.
Meine Hand fängt an zu kribbeln.
Ich gehe wieder in mein Auto und muss dort erst einmal durchatmen und mich wieder aufrecht hinsetzen.
Und dann fahre ich direkt heim.
Das war es.“

Marlene lachte: „Geil.
Nett.
Eine Toilettenfrau.“

„Pling“, kam es aus Marlenes Laptop.
„Arma hat geantwortet: Drei Tage!
Ich hab es dir gesagt.
Die sind super die Jungs!
Es ist so gut, dass es sie gibt.“

Fred: „Und Mädels.
Na, die sind jetzt alle am arbeiten.
Da gehen jetzt drei Tage lang keine Lichter aus.
Darauf kannst du wetten.“

Marlene: „Und dabei freuen sie sich wie Schneekönige ... endlich mal echte NSA Daten.“

Fred: „Sie tanzen im Kreis ...“

Marlene: „Weihnachten und Geburtstag an einem Tag.“

„Pling“, kam es wieder aus Marlenes Laptop.

Marlene: „Und Werner Koch auch.
Auch drei Tage.
Guck: ein Herzchen von Werner.“
Sie schob ihren Laptop zu Fred.
Marlene: „Der ist sonst nicht so emotional.“

Fred: „Wow!
Ein Herz von Werner ...“
