## TRON Attack

<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Lasse and Sigur entered</span> their game room.
Game rooms were large and round, with plants, many painted fantasy pictures on the wall and a pedestal with drinks and many kinds of fruits, nuts, and sweets.
Four large computer screens floated above a table in the middle of the room.
On the table lay two keyboards, four controllers about the size of a fist and two large glass spheres that were open towards the bottom.
Lasse and Sigur sat down on the chairs and took the keyboards.

"9:30 a.m.! Here we go." Lasse shouted.
Both sat in front of their monitors and looked at the activity display.
Sigur nodded his head firmly.

Lasse: "Okay.
It's quiet.
Let's get started..."

He screamed.

Lasse: "S H I T! Sig! Close the firewall, we're getting a massive attack, quantum parallel."

Sigur quickly hacked his keyboard four or five times with his right hand and fixed the screen with his eyes.

Sigur: "Done! It's closed! Who was that?"

Lasse shruged his shoulders.
"I don't know."

Sigur: "WHHAAAATTTTTT????!!!"

Lasse: "What?"

Sigur drummed on his keyboard: "Shit! Shit ... shit!"

Lasse: "Say it."

Lasse stormed over to Sigur's computer:

Lasse: "What's wrong?"

Sigur: "The firewall is open again! Just like that... And I'm... out, gone, can't do anything.
Not even typing.
Nothing. Nothing."
He punched his keyboard with his fist.

Lasse: "Shit!"

Sigur: "Fuck."

Lasse: "Pull the plug!"

Sigur: "We can't do that!
Against the rules."

Lasse: "Fuck it.
...Okay.
Okay, good, good.
Then don't."

Sigur threw away the keyboard, stood up and hit the wall a few times with his fist.

"What was that?" Lasse asked.

"Who was that?" Sigur asked, with his face still toward the wall.

Klack! The door opened.
Both of them turned to the door at the same time, where Lilly walked in with a broad grin: "Hi, hi! Too slow, boys..."

Lasse: "Oh, no! Don't. Don't.
Please don't... Not again..."

Lilly was Lasse's little sister and turned 12 two months ago.
She had only been playing TRON for a year but had a real talent for it.
During the first game, she had already found the documents that Edward Snowden former employee of the largest American secret service, the NSA, had taken with him and which had caused a worldwide turmoil at the time. 
TRON immediately added some missions to catch Lilly and many players accepted this challenge.
But she had managed to survive and to keep the documents until the end of the game.

<div class="infobox" id="snowden-files"></div>

She closed the door again.
Lasse and Sigur looked into each other's eyes for a long time and then, without anyone saying anything, went into the courtyard to the table tennis table.
Sigur took two bats and threw one of them at Lasse.
