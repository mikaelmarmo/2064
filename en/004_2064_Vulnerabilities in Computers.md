## Vulnerabilities in Computers

<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Lasse and Sigur sat</span> at the large dining table of the kitchen.
Both with a large cup of thick cocoa in their hands.
Lilly sat cross-legged on the other side of the table, facing toward them, still smiling.

"You prepared that," Lasse snorted at her.
"That went too fast.
That was a computer program, wasn't it?"

Lilly: "Sure! I was writing on it the whole evening yesterday.
This morning I only entered one command and ..."

Sigur: "But how did that work? I had it all sealed up.
I went over everything again yesterday and this morning.
Everything was fine.
Where did you get through?"

Lilly: "A still unknown vulnerability in your firewall.
A large American secret service had it installed and paid a lot of money for it.
It works great.
You can have a look at it after the game in the log files, everything is in there.
First, I'm going to do some more raids with it.
You can get into a lot of computers through that backdoor.
It's widespread, and no one knows it." She grinned broadly.

<div class="infobox" id="vulnerabilities"></div>

"That's unfair!" grumbled Sigur.
"You know a backdoor, and you don't tell anyone.
There shouldn't be secret backdoors in the game!
That's stupid!
How can you protect yourself against that?
I really did a lot of work, there wasn't a gap left."

Lasse: "I don't think it's allowed to keep backdoors to yourself at all.
That's against the rules.
Backdoors should be known by the public, they belong to everyone like water and air.
It's a human right... I learned it in my computer administration class.
It's also a Cypherpunk rule. You can even lose your right to work in this field if you don't make a vulnerability known."

Lilly: "Today! Today it is like that.
It was allowed back then. At least, I think so.
In any case, it was totally common."
She smiled.
"I've read an e-mail where someone has offered a unknown vulnerability for sale.
For an awful lot of money.
And somebody bought it."

"Bullshit!" Sigur shook his head.
"Sell vulnerabilities?! Maybe when you're playing Mafia, but not otherwise.
That would be totally corrupt.
Superbad.
That would be as bad as making people sick and then making profit on medicine."

Lilly pulled her eyebrows together.
"Pfff.
Sure, I'm sure.
Hey, wake up!
The world was different then.
Everyone did that back then!
Governments, corporations, intelligence agencies.
They've built whole vulnerability arsenals, as well as weapons arsenals.
They were hoarding them, masses of backdoors."

Both boys went off and snorted.

"Lilly, that's bullshit," Lasse said, still laughing.
"Who told you such nonsense?"

Lilly crossed her arms and made a serious face: "Nope.
I'm serious. Masses, mountains... I'm telling you.
I have..." She stopped.

Lasse made a calming hand move: "Lilly! Be real!"

"You have...?" Sigur asked.
"You have ... what?"
He slipped over to her.

Lilly looked down.

"You found more weaknesses?"
Sigur asked and looked at her intensively.

Lilly looked at him: "Pfffff.
Plenty.
Incredible stuff.
You can't imagine what it's all about," she grinned.
"I found a database at the NSA: 'Zero Day Exploits,' full of vulnerabilities nobody knows yet.
Marie's an agent there. She helped me get to it.
She is allowed to look everything up in the database, but she is not allowed to do anything with it that does not fit her mission, and everything she does and says is recorded.
Everything, every keystroke, every conversation, every facial expression.
They're really recording everything! Of all the employees.
Stupid.
It sucks to work there.
But I could get the database out of there with her help!
It was not quite easy ... And now I can ... Uuuuuhhhhh", she shook her head, "... do so much ... 
Hi, hi.
... I know it's unfair, but it's a game.
You have to be able to lose."

Sigur grumbled: "You work with the NSA? They're enemies.
That's against the rules.
And Marie gives you the information because you're friends in real life, too."

"Not at all.
I cracked two computers for her from hackers. She needs them for her mission.
She has almost finished it.
The two hacker girls just have to be convicted and get more than 10 years.
They are already on trial right now.
When that's done, she's finished the mission.
That was the deal.
We just worked well together as women, we didn't cheat!
You can do a lot of other things, especially a lot of psycho stuff: You can blackmail, recruit, bribe, intimidate, lie.
You know what that world was like?
I mean, how are you gonna get a flying combat robot without having someone in the CIA doing what you want?
No wonder you haven't got one yet.
Marie already has one."

Lasse and Sigur looked at each other.
Flying combat robots.
That was their ultimate goal.
They didn't know anybody who had managed to take one over yet.
Sigur grabbed his stomach and said, "I'm gonna be sick."
Lasse pinched his lips together and looked over to Lilly: "How can you turn someone over, that's working for the CIA?
In the combat robots division?"

Lilly: "I don't know how Marie did it.
But you gotta figure out how they think.
I mean, she has access to all communication data.
She sees what kind of emails someone writes and receives, who he meets or talks to on the phone, what he buys, whether he has enemies, a relationship of which his wife is not allowed to know anything, debts, and then she looks for something that she can blackmail him with.
She apparently found something.
Or maybe someone is just really unhappy with his job.
Then you might be able to recruit him.
Marie, for example, is unhappy, she just got a job she doesn't want to do at all.
But she has to.
And she can't even walk away from there.
You can't just do that in intelligence.
Maybe you can try and recruit her for something.
But then be extremely careful.
You really shouldn't get caught there.
If you do, all hell will break loose.
Half the Secret Service will be after you right away.
And zack you have a rocket in the room and see your medal of honor on the screen.
Like Lars last time."

Sigur: "Lars cheated.
That's why he got kicked out.
Blackmail and all that isn't part of the game.
TRON takes you out of the game when you do something like that."

Lilly: "TRON doesn't.
Marie can access her combat robot at any time and see what it's doing.
And you can't do that without blackmail.
It's clearly part of the game.
That used to be called social hacking. 

Sigur was silent.

Lilly: "If you don't want that, you can play Cypherpunk missions.
They're not about taking over as many computers as possible or catching hackers, but about bringing things to light, making up for injustices and allowing more and more people to escape the oppression that reigned at the time.
So you have to think more like we think today.
Then maybe you can find out how the Cypherpunks won the war.
I mean, they were forces to be reckoned with then, the intelligence agencies, the armies, big corporations.
But in the end, they made it.
I was wondering if it would be exciting to play Cypherpunk.
Maybe that's the whole point of the game: that we can learn what the liberation of the Internet was like."

Lasse and Sigur looked at each other.
Lilly was just about to tell them what the real purpose of TRON was, the game they had spent hundreds of hours playing, looking at everything, trying out, learning everything they came across.

Lasse: "Hey, Lilly.
I've been playing TRON for three years now, and in the end it's about conquering the world, determining politics, changing the course of history, but not about the liberation of the Internet.
I didn't want to play Julian Assange in order to break the surveillance system of that time, to abolish secret services, to make governments transparent, but because he has a lot of cool people around him, who have a lot of skills, and because half the world is after him.

Lilly: "So, did you get the part?"

Lasse: "No.
You know that."

Lilly: "You can't get it if you don't fill out the questionnaire at the beginning in such a way that your main concern is the future of the Internet.
Julian is a Cypherpunk.
If all you want is adventure, you can't get it."

Sigur was hissing in between: "HOW DO YOU KNOW ALL OF THIS?"

Lilly: "I have read Marlene Farras: The History of Cypherpunks, before I even started playing TRON.
She writes about 2021 and after that, until the end of the war.
How it was then, how she learned to hack and then became a Cypherpunk.
And she writes about what it was really about, how people in the secret services thought and in the companies and how the hackers were, with white, grey and black hats.

<div class="infobox" id="hats"></div>

And then the hacks that they did.
Wow! That was awesome.
So I thought I'd try out what she wrote in TRON.
And that was just like it in the game.
It was all there.
Edward Snowden.
It was just like Marlene Farras wrote about it.
It's exactly how she described her own hack at hacking the vulnerability database of the NSA.
And I did that together with Marie.
It was complicated, but it worked just like it really was.
It takes an NSA operative, even a specific one.
And that's how we got all the vulnerabilities out."

Sigur: "All of them? - How many are there?"

Lilly: "I don't know.
It's a complete mess.
I can't imagine that they themselves had an overview of it.
There were a lot of snobs and bores working in these secret services, like in all the big government agencies.
But all in all, there must be thousands, maybe tens of thousands of vulnerabilities, for all the computers you can think of, including satellites.
And one single vulnerability is just the back door I just walked in on you guys."

Lasse: "You can give us our computers back too..."

Lilly: "Nope.
"Firstly, she's already got Marie and secondly, that would be cheating.
"For you, it's time to escape."
