## Where's Marlene Farras?
<div class="_202x">
    <span class="year">2021</span>
    <hr class="hr" />
</div>

<span class="first-words">In the headquarters of the CIA</span> in Langley/Virginia, a spacious office with a wide window front.
A woman and two men in business attire sit opposite each other at an oval meeting table.
In the middle a coffee pot with some cups, biscuits and a metal box for mobile phones.

Woman: "Dahab and Hurghada!
Egypt."

First man: "Is the information good?"

Woman: "Definitely.
The audio scanners picked up her voice from a smartphone in Dahab and from one in Hurghada, each less than 10 meters away from the phones.
Only these two times, though, but without a doubt.
In Hurghada she ordered a pizza."

Second man dissatisfied: "Only two records?
That's not possible."

Woman: "We know she's there!"

First man: "Is there video footage?"
He offered her coffee.

Woman: "Thank you.
No.
She's careful.
We looked through the local surveillance cameras and some smartphone cameras close by, in a time frame of two hours and half a mile diameter.
There was nothing."

Second man: "Not even something?
A woman in a burka, alone, with a western walk?"

Woman: "Nothing useful.
There is Tor and I2P traffic in both places.
But not much more than usual.
And nothing that can be assigned to her.

<div class="infobox" id="i2p"></div>

Second man: "She doesn't go into the Tor network the usual way.
You won't get her there.
You have nothing else?
That's it?
What about Jabber?
Ricochet?
Pond?
Tor Hidden Services?"

<div class="infobox" id="jabber-etc"></div>

Woman: "She doesn't use Jabber.
We didn't find a Hidden Service connected to her, Ricochet and Pond we do not know.
They are problematic.
They are too small.
The team meant it would be too much work."

The second man laughed out loud: "Too expensive!
Too expensive?
You are the NSA!
After so many years, you can't get into programs that a school dropout and a Google programmer wrote in their spare time?
Why didn't you turn them off long ago?"

The woman closed her eyes and struggled with her anger: "Can we please stay calm?
Ricochet and Pond run distributed in the Tor network.
Ricochet doesn't have a central server at all, so we can't shut down anything, and Pond's servers are decentralized.
When we shut one down, we get a WikiLeaks effect: then three new servers pop up elsewhere.
My daughter could set up a Pond server in 20 minutes.
It's not that easy."

First man: "And Tor itself?
We know much about Tor.
You could turn it off, or at least jam it, make it really slow."

The woman moaned: "We can't shut Tor down, nor jam it.
Not for now, at least.
Too much depends on it."

Second man: "You've lost RAPTOR, you've got hackers in your internal communications, so Tor is the safest thing you've got out in the field!
You need it yourself.
You have nothing better.
That's the truth.
And that's shit!
This is embarrassing!
The NSA needs an open source program to communicate safely."

First man: "Hej, stop!
This is not about us blaming each other.
We hunt terrorists.
Together!
9/11, remember?"

Second man angry, getting louder: "I need good information for my work!
My mission is to stop Marlene Farras, and I won't let any morons get in my way.
Not even when they work at NSA."

Woman: "Don't mention names!"

The second man stood up, full of rage: "I heard enough.
We meet here in person at CIA headquarters!
E-mail, telephone is too insecure for you.
My smartphone is in a tin box, I'm not supposed to mention names.
And I'm fucking here at the headquarters of the most powerful intelligence agency in the United States.
Who are we afraid of?
A few pubescent boys and girls in sloppy sweaters.
We here at the CIA are standing at the frontline, we don't just click us through networks and push some bits and bytes from one corner to the other.
We do real work, we do dirty work.
When I tell a CIA field officer to put his smartphone in a tin can, he throws it on the floor and steps on it.
And then he tells me he needs a better one."

The first man grabbed him by the arm to hold him back.

Second man: "I'm really tired of this shit.
I'm about to call Academi.
And they certainly won't talk so much about all the problems they have."

Woman: "Academi is a mercenary company, not an intelligence service.
They have combat troops, special units.
You can send them when we know exactly where she is." 

<div class="infobox" id="academi"></div>

Second man: "Nah, nah.
You're wrong.
Meanwhile they also take search orders.
They now even have their own hackers.
They can't always wait until they get good information from somewhere.
When I tell them that Marlene Farras is in Hurghada or Dahab, it takes two days and they have her."

Woman: "And then she's dead, and some other people also.
We don't use Academi for hackers.
Too unclean.
The situation is heated up enough."

The second man made a move to answer, but then leaned back in his chair, pinched his lips together and crossed his arms.

Woman: "I think that's it for today.
I would also perfer to have more information.
The XKeyscore code for the relevant data is here."

<div class="infobox" id="xkeyscore"></div>

She handed them two handwritten notes.

