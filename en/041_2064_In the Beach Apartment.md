## In the beach apartment 

<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Sigur and Lasse sat</span> in the living room of the beach apartment in two huge armchairs.
From there they had a 270-degree panoramic view over the ocean.
On the other side of the room there was an oversized TV, behind it, on the left an open fireplace.

Sigur: "What do you think, when will he come back?"

Lasse: "No idea."

Sigur: "Maybe he won't come back at all, maybe he'll just get some CIA agents.
In any case, he's watching us on some monitor or hearing us through some microphone.
And I haven't found my RFID chip either."
He was stroking his pants again.
"This is really not a good place here.
Let's go."

Lasse: "There's something to do here.
Otherwise Marlene wouldn't have sent us here.
This new secret service is somehow connected to him.
But how?"
Lasse looked around.

Sigur: "Sure.
His main cloud computer is probably somewhere around here.
The one with all his information customers on it.
And one of the customers is the LBI.
Easy.
Now we'll just hack into it."
He raised his eyebrows and looked at Lasse.
"And since he knows that we are hackers who are capable to permanently hack into a CIA system, he lets us do our thing alone in peace."

Lasse: "Mh mh.
That seems quite unlikely.
But then again, these non technical guys sometimes believe they are safer than they are ...
At least he watches and listens to every step that we take ..."

Lasse grinned sarcastically: "We could just ask him, where he has his computers and how we can get in.
What do you think?"
Sigur didn't react.
He seemed to think intensely.

Suddenly Sigur looked up surprised.
He reached into his backpack and took out his laptop.
He put a cloth over his hands and laptop, and entered his password.

Lasse: "Don't you do 2FA?"

<div class="infobox" id="_2fa"></div>

Sigur pulled his eyebrows together and pointed to a USB stick on the side of his computer.
Then he took the cloth away and started typing.
Lasse watched him for a while, Sigur was absorbed in his device, unresponsive, "plugged in", as they called it in their school.
After a while he also took out his computer, turned it on, entered his password and went online.

"Ping," said the chat program and showed a message from Sigur.

<div class="terminal">
Sigur: "Finally.
There you are! Make sure nobody looks at the monitor from behind."
</div>
Lasse drew the screen slightly to himself.

<div class="terminal">
Sigur: "Next to your left foot under the table is a power socket.
I have a wireless plug under my left leg.
Take it carefully and plug it in there."
</div>
Lasse pretended to tie his shoes and pushed the plug into the socket.

<div class="terminal">
Sigur: "Good.
I'm connected.
The device detects ultra-small fluctuations in the power grid, for example of the activity of all the computers connected to the power grid at this place here.
You can even recognize the individual keystrokes of computers.
The NSA once stole a password of mine with this.
Never type a password while plugged into a power socket.
I want to run a portscan on all computers in Caracas and see when something is happening.
Maybe we'll get something."
</div>

<div class="infobox" id="portscans"></div>

<div class="terminal">
Lasse: "Come on, if he has computers here at all, he's probably so paranoid that they're connected to their own power grid.
Probably he's also going out directly to the Internet via satellites."
</div>

<div class="terminal">
Sigur: "An own powergrid?
Yes, maybe.
But that means only that there is a filter between both.
And my device is better than most filters.
And I also scan the computers that connect to the Internet via satellite." 
</div>
He sat in front of his computer for a while and typed from time to time.
Meanwhile, Lasse was working on his Pond news.

<div class="terminal">
Sigur: "Got it! Three computers are right here in the power grid.
Probably in the basement.
Oh!
Deep in the cellar.
At least 10 meters deep.
One seems to be a Tor hidden service, probably on a cubes system.
I'll take a closer look."
</div>

<div class="infobox" id="cubesos"></div>

<div class="terminal">
Lasse: "I just sent you Lilly's list, what she uses to hack into Cubes.
There's nothing for the latest version, though, but maybe you can use it."
</div>

<div class="terminal">
Sigur: "No Zero-Days for the latest version?
Good sign!
I love Cubes.
Hey hey.
And our man here doesn't have the latest version.
That's great.
I'm just poking around a bit.
Lilly is an angel."
</div>

A few minutes later: Sigur didn't move on his armchair and wrote: 

<div class="terminal">
Sigur: "YESS!!! I'm in! Super simple.
He had quite good protection, but not the latest version.
I always say: update, update, update.
The setup looks like NSA somehow.

Oh!
What is this here?

A Tor hidden service.
Nice.

With the onion address idtAIrLBI0KJ6gjyro8uTkHa.onion.
And what does he have on that ...?

Aha, a shop.
Wow!

He is really an information trader.
He was not lying.
Nice user interface.
Professional.

And here: The search program he was talking about.
You can enter telephone numbers, e-mail addresses.
Like XKeyScore from the NSA.
Let's see what it can do.

Come we try an email address ... no, not on of ours! For God's sake.
For example, Edward Snowden's."
</div>

Lasse opened his Tor Browser and entered the onion address.

<div class="terminal">
Lasse: Wow!
It works.
Here Edward Snowden's data:

Edward Snowden

Activities: Hacker, Internet Activist, Internet Security

Last confirmed location: 2 days old 

Geo location profile: 2% coverage

E-mails total: 12234 (all address data, subject line)

E-mails without PGP: 335 (full content, all address data, subject line)

Jabber contacts: 71

Financial Data: 12334

Portrait photos: 72

Communication contacts: 781
</div>

<div class="terminal">
Sigur: "Shit, he has only 2% coverage in the geo location profile.
How does he do that?
And look what the data costs: Full data $65,000, metadata $38,000.
That's a bargin.
Let's see how much coverage Marc Zuckerberg has and what it costs ... Ahhh. He has 99.3%.
Wow.
Of course, all tracking on.
And his data costs: $ 81,000.
Why is he more expensive than Edward?"
</div>

<div class="terminal">
Lasse: "Hmmm.
The people who are interested in him have more money?"
</div>

<div class="terminal">
Sigur: "Never mind.
I'll keep looking. There's certainly more to find on this computer."
</div>

