## Night-Owl

<div class="_202y">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Marwin sat in the </span> meeting room at his table.
Before him lay five paper planes and two ships.
He was humming "Don't worry, be happy" quietly.

The door handle slowly moved down and the door opened.
A slim young man in prison clothes came in, in his early 20s, looking a little shy.
He sneaked past Marwin and sat down on the chair opposite.
He had neither handcuffs nor ankle cuffs, but bruises were visible.
They both looked at each other.
They didn't speak for a long time.
At some point Marwin reached out his hand to him.

Marwin: "Marwin."

He took Marvin's hand.

Amon: "Night-Owl."

Marwin scared: "Not the hacker names, not here.
They listen, they record everything."

Amon: "You already know that."

Marwin: "You really are Night-Owl? The Night-Owl? The security camera hack?"

Amon nodded slowly.

Marwin: "Wow.
I've learned so much from you.
How long have you been here?"

Amon: "Two years, ten days, five hours."

Marwin: "Shit.
Then they took you the same day as me.
Berlin?

Amon nodded again.

Marwin: "Boah.
Berlin is a big city.
If I had known that Night-Owl is in Berlin ... Wow!"

Amon: "I used to think the same: wow, Berlin.
But it's not like that anymore..."

Marwin: "Not anymore?"

Amon: "I don't want that in here anymore, I don't want that out there anymore either.
It's all so fucked up.
They're better to you if you don't care about anything."

Marwin: "They want to let me out!
I think today.
Not you?"

Amon: "Forget it.
They're just tricks.
They've let me out three times already.
I signed papers, promised that I wouldn't say anything - until the end of my life, otherwise they would bring this end of life forward.
That's exactly what they said.
And all I wanted was to get out.
I would have signed everything ... And then I stood three times already outside at the bus stop, with all my things and then came no bus but a jeep and they got me again.
Meanwhile I say everything they want to hear ... It doesn't matter anyway."

Marwin: "You have to keep something.
Something they can't have.
The truth.
You mustn't lie!
Not even a little.
Or you'll take her side..."

Amon, the tears came.
He took his hands in front of his face: "I can't do this anymore.
I'm going crazy here.
Really crazy.
Of course I'll take her side when I get out of here."

Marwin took his hand: "Don't lie!
Otherwise, you're really going crazy.
They're crazy because they lie so much."

Tears rolled from Amon's cheeks.
He just let her go, didn't wipe her off.

Marwin: "What did they tell you to do here?
With me?"

Amon didn't react.

Marwin: "You know this is a game.
They always play games.
Come on, write it here on the note and cover it with your hand."

Amon looked briefly in the direction of the surveillance camera in front of him and then took the note.

Amon wrote, "I don't know.
They said you went crazy and didn't want to get out of here.
And you need someone to talk to."

Marwin read the note and looked at him confused.
Amon shrugged his shoulders.

Amon wrote, "Don't you want to get out?"

Marwin: "Of course I want to get out of here.
But there's some lazy game going on.
You're acting all twisted.
Probably because of Marlene."

"Marlene?
Marlene ... Farras?" wrote Amon.

Marwin nodded and wrote, "My sister."

Amon: "Waaas??
Marlene ... is your sister?
That's awesome.
I played with her at a hackaton once.
She's so incredibly fast."

Marwin: "She hacked a TAO computer two years ago.
I guess that's why I'm here."

Amon: "She's here too?"

Marwin: "No.
She's free.
Still.
After two years!
Cool, isn't it?"

Amon: "How do you know?"

Marwin said out loud, "I know it."

Then he took the paper and stuffed it in his mouth.

Marwin swallowed it and grinned: "It's not even that bad for the stomach.
Now they have two, three hours to get it out if they want to read any more of it."

Amon had to laugh.
"It's so good to have a man in front of you," he thought.
"I almost forgot what it was like."

Marwin said at normal volume: "Night-Owl, when we get out of here, we'll do a hack together - No, no, fun ... We'll be good."

Marwin wrote on the next sheet: "We have already made one together.
Your 'Bachelor thesis', that's what you used to call him.
But please, don't write my real name."

Amon laughed all over his face.
He wrote, "You?
It was you?
Wow. Wow.
I always wanted to meet you for real.
That was so awesome in there.
They didn't stand a chance against us at all.
Not a trace."

Marwin: "Not a bit!"
Tears came to his eyes.

Amon: "That was so awesome!"

Amon pushed the paper away and said: "Why do they do something like this?
Why are they locking us up?
Why are they yelling at us, hitting us with squares of wood against our heads, withdrawing our light, telling us that our family is being murdered?
Why do they seem to let us go, not let us sleep and give us broken computers with fake internet?
What's the matter with you?
Why are there such assholes?"

Marwin: "They know nothing of our world.
They know nothing of what it means to really live according to one's own ideas and not according to what others command or pretend or have persuaded them to do.
They think it's naive when people want to stand on their own ground.
They think they need friends.
And you can't have any friends when you need them.
And they don't even understand why.
They think that it is naive of them not to adapt to the system, that they can't do anything anymore, that they don't have a high school diploma, that they don't study, that they don't get a bad curriculum vitae that says that you can't rely on them or that they don't work well.
Without all that, they couldn't live, they think.
They couldn't afford a house anymore or a car, a family, if they didn't do a job where someone told them what to do.
Even if they are bosses of big companies, the shareholders tell them what to do.
It's so sick.
They are adult people and they think they have to bow, otherwise their lives won't work out."

Amon: "This is really sick!
And we didn't hurt anyone.
There's no harm in turning off or converting a few thousand surveillance cameras, or using the giant advertising displays for a few days to play Tetris.
Do they have to lock us up for this?
Okay, I hacked the V people database at the BND.
That was stupid.
That's when they went wild.
But I had such a fucking rage because so many people around me get money to listen to me.
And it would be better for humanity if all the V people were to become public.
For her, too.
They're gonna get sick.
Like Soeren.
It's a shitty job."

Marwin: "Soeren?
You did?
Yeah, right.
You really had the V people database?
Oh, shit.
Why didn't you pass it on?
I'd have liked to have that, too."

Amon: "I only had the file encrypted with VeraCrypt.
But I had already installed a small guard program on the BND computer, which sends me the password as soon as someone had opened it.
But nobody has, not in two months."

Marwin: "I really would have had a few questions for this database in Berlin ..."

Amon: "So did I.
Not any more.
And you're really not here for your own hack?"

Marwin took the sheet and wrote: "I don't think so.
I didn't get any questions about the things I was in on."

Amon said out loud, "Shit! They're just shit."

A door opened and a soldier appeared.

Soldier: "Night-Owl, come with me!"

Amon looked at Marwin briefly, rose and followed him outside.
Marwin closed his eyes and said quietly, "Shit!
That was a trick."
He distorted his face, wiped the tears from his eyes, then stuffed the leaf into his mouth and began to chew.


