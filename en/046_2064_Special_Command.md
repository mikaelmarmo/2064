## Special Command

<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">In the games lab.</span> Lasse and Sigur stood opposite each other with light balls on their heads in front of their floating monitors.

Lasse took his bullet and put it on the table in front of him: "Sigur!
Do you sleep too?"

Sigur also put the bullet down: "Yes.
I'm dreaming.
But nothing special.
Quite nicely done by TRON.
Nice and colorful.
You?

Lasse: "With me suddenly everything turned black.
That's funny.
Totally dark.
I can only hear a wobbling sound, very quiet.
A little like drugs.
But I can't see anything.
It's all dark black.
Smelling and tasting are gone too."

Sigur: "Hmmm.
All gone?
I haven't had that yet.
I don't know what that is.
Now something seems to be happening to me.
Dream images change.
Whoa.
kick ass ... Oh! Oh! Oh!
I have to put the bullet back on.
I wake up ... in 30 seconds."

The door to the game room opens.
Edgar, your communication and movement teacher came in.
Both of them turned to him.
He put his finger on his mouth and told them to keep playing.
He went to Sigur, stood close to him and laid his hand on his shoulder.

Sigur turned to him and said, "Is this going to be something awesome?"

Edgar nodded: "But you can just keep playing.
It's gonna get a little weird.
You can go all the way in there.
I'm here."

Sigur breathed deeply and delved into his dreams.

It was 5:45.
Fortunato sat down alone in the living room of the beach apartment and turned a magic cube.
He listened to a soft sound of a prolonged vehicle.
He rose, went to the front door and opened it quietly.
Seven men from a task force entered the apartment fully equipped with assault rifles at the ready.
The last one carried two big medicine cases.
Fortunato led them quietly up the stairs.

Fortunato to the group leader: "Right is Lasse.
Left Sigur.
Lasse is in deep sleep, I gave him gas.
Sigur sleeps normally."

The group leader quietly opened the right door and made a sign to the last soldier.
He carried the medicine cases into the room.
He knelt next to Lasse and checked his breath and pulse.
Then he turned Lasse's head to the side and put some kind of vaccination gun on the back of his head.
He closed his eyes and pulled the trigger.
Another short, dangerous-sounding "Plick", he put the pistol to one side, wiped away with a small cloth the blood that came out of the tiny wound.
He stuck wound glue on the spot.
Then he reached into the suitcase and pulled out a hand-sized device with a screen.
He turned it on, pulled out two antennas and briefly observed the pulsating signals that appeared on the screen.
He gave the squad leader a sign that everything was okay.

The group leader turned to Sigur's door and counted down from three with his fingers.
At zero, the first soldier entered the door and four more stormed into the room with a loud roar.
Two soldiers brutally turned Sigur on his stomach and fixed his arms behind his back with plastic cuffs.
One soldier held his weapon directly to Sigur's head, another held a strong lamp in his face.
Sigur blinked his eyes.
He had a numb feeling in his head, the light, the screams, the brutal hands on his arms and legs, the many soldiers around him, all that was not quite there for him yet.
Then he heard a voice calling from afar:

Group leader: "Mr. Alvas!
Mr. Alvas!
Can you hear me?"

Sigur turned his head to him and looked at him with one eye.

"Mr. Alvas!", the group leader took a chair and sat next to him.
"Listen to me!
Do you hear me?
Nod when you hear me!"

Sigur didn't react.
One of the two soldiers holding him turned his arm.
Sigur moaned painfully and tried to nod.

Group leader: "Okay.
Mr. Alvas, you've done some very stupid things.
You're supposed to know you're stepping on people's toes who won't let you get away with this.
Maybe you just ignored it, maybe you didn't.
Whatever.
If you believed that you could fuck us with impunity, that you could penetrate security systems with impunity and take them over, then you were wrong.
Obviously.
Otherwise, I wouldn't be here right now."

Sigur in pain: "Who are you?"

The group leader gave a sign to a soldier next to him and he hit Sigur firmly on the head with his hand.
Sigur screamed.

Group leader: "_ICH_ ask the questions.
They wait and give answers.
- I could ask you a few questions now to find out a little more about what you've done.
Unfortunately, we have no known gaps in your knowledge.
We know everything we want to know about you.
And that's a lot.
I'm not here to ask questions.
I'm here to make a deal with you.
Naturally, in the situation in which we find ourselves, the deal will naturally be rather one-sided.
There won't be many advantages on your side, but at least one:
You'll survive this.
Otherwise it's more of a commitment.
But you'll sign anyway.
Not with a pen, not on paper, and not yourself.
We'll do that for you.
And it's in your body with a little ball like that.
You see?"

He held a bead about five millimeters in diameter in front of Sigur's face.
He had turned away and didn't react.
He received another blow to the head and then turned to the group leader.

Group leader: "Such a bead, a marvel of technology and a nice invention from our torture chambers, ah, excuse me, from our rooms for advanced interrogation techniques.
There's a radio controller built in and a multifunction processor.
And we programmed it to do two things first and foremost.
First, it reports your whereabouts and other body data such as heartbeat, blood pressure, etc. on request.
and second, he can burst the capsule at any time, and then about half a gram of potassium cyanide, which is prussic acid, enters her body.
Thereupon you experience a death struggle lasting about one to two hours.
And that's one of the most unpleasant things there is.
There you think, you will be torn ... into small pieces.
Oh yes, and if you should come up with the idea of going into a room shielded from radio interference: After about twenty minutes without radio contact, the capsule also opens.
She needs contact with us.
We always want to know where you are and how you are doing.
Now, no more than 20 minutes without radio contact, remember that.
We don't want you to die, do we?
Then we wouldn't be going to such lengths with you."

Sigur tried to free himself, tore his shoulders back and forth, kicked his legs.
The soldiers forced him onto the bed.
He almost lost consciousness in pain.

Group leader: "We can do this with pain, or relatively unspectacular.
Your choice.
What do you want?"
He bent close to Sigur's ear.
"Hmm?"
He gave another sign and the soldiers turned Sigur violently on his back.
They fixed his body to the bed with wide adhesive strips.
The soldier with the medicine cases came, put them in front of Sigur and opened them.
Then he took a chair, sat down and took out a syringe.
He stabbed her in the side of Sigur's stomach.

Soldier: "This is a local anesthetic.
You won't feel any pain."

He opened Sigur's pants and pulled up his shirt to expose his stomach.
Sigur fidgeted.
Then he saw the medical soldier stab once into his belly with a long, slender scalpel and then press a ball into the bleeding gap with a pair of tweezers.
Sigur estimated about four to five inches deep.
Then the medical soldier desolated the wound with a pen and wiped away the blood.

Medical soldier: "You'll have a few days of pain, a bruise, but it won't be life-threatening.
The capsule has small barbs that make it impossible to pull it out without breaking the barbs.
If they abort, the cyanide will also leak.
So be careful with that.
A punch in the stomach or sport, however, is not enough.
The hooks react very flexibly to pressure.
Only pulling doesn't work.
So you better not do yoga."

Sigur got sick.
He wanted to vomit.
His body twitched back and forth.
He himself had actually already given up.
But the body still resisted.
He could just turn his head to the side before the gush of vomit splashed out of his mouth and hit the floor in front of the bed and the group leader.
He jumped up.

Group leader: "Devil!
Watch your step!"
A soldier hit Sigur on the head again.

Group leader: "That's disgusting!
Uah."
He looked at himself from top to bottom and then took the towel that the medical soldier handed him.
He started wiping himself with it.
"Piece of shit."

The medical soldier took out the measuring device again, checked the measurement data of the bead in Sigur's stomach and then wrapped it up again.

Medical soldier: "Ready!"
He threw the rest of the stuff into the suitcases, closed them and left the room.
The other soldiers followed him.
The group leader was the last to stand alone in the room, looking at Sigur:
"Don't fuck around, we'll be watching you.
Three men will be watching your monitor around the clock."

Then he taped tape on Sigur's mouth and left the room.

Sigur whimpered.
He tried to free himself, but he couldn't.
He tried to scream, but he couldn't bring out more than a dull tone.
Lasse seemed to sleep so deep that he didn't notice anything.

Only a few hours later did Lasse wake up.
He heard strange noises from the next room.
Squeaking, rumbling.
His head boomed like after an excessive beer evening, at least that's how he imagined it, because he had never really been drunk before.
He stood up and felt a pulling pain in his head.
He staggered.

"What's the matter?" he thought.
He went to Sigur's door, opened it and saw him tied to the bed, with his head red, wriggling.
Lasse jumped to him, tore the tape from his mouth and opened the plastic cuffs with a knife he found in Sigur's backpack.

Sigur straightened up and sat down on the edge of the bed.
He breathed heavily.
"They bugged me.
The assholes.
Fortunato!
I have a capsule in my stomach.
You said there was cyanide in it."

Lasse: "Oh, shit.
God, no.
He sat next to him.
No, fuck.
Who was that?"

Sigur: "A Swat team with doctors: five to eight people.
I can't remember.
It was like a movie."

Lasse: "Was Fortunato there?"

Sugur: "What are you asking?
Of course it's not.
But that was definitely true!"

Lasse: "But they didn't take you with them!"

Sigur: "No, apparently not."
He looked down at himself.

Suddenly they heard a soft whimper from the lower floor.
They both listened.

Lasse looked carefully around the corner, down the stairs into the living room.
Fortunato lay on one of the armchairs, tied to his hands and feet, with tape over his mouth and a similar red head as Sigur.
Lasse jumped there and freed him.
Fortunato let himself sink into the armchair and rubbed his arms.
Sigur joined in, and so the group sat around the living room table like the night before.

Fortunato, breathing heavily: "With you we experience things!"

Lasse and Sigur looked at each other.
Sigur closed his eyes, shook his head slightly and hissed to Lasse: "I just want to get out of here."
Lasse nodded.

Fortunato: "I haven't had a raid in five years!
I know people here.
Nobody's raiding my place.
They were Americans."

Lasse: "That wasn't a raid.
It was an intelligence operation."
He and Fortunato looked at each other.
Lasse looked at the living room clock.
"12:15.
Shit.
Shit!
Our plane's gone."

Fortunato: "When did he leave?"

Lasse: "He leaves at 12:35."

Fortunato: "No problem!
You want to get out of here?
I can still do that."

"Yeah, get out of here," Sigur said with a dark face.

Lasse: "But it's not enough anyway.
We have to go there and check in and..."

Fortunato: "The airport is just around the corner, 30 minutes.
I'll postpone departure by an hour.
Then you won't have to hurry."

Lasse looked at him.
Sigur nodded and Fortunato grabbed the phone and phoned with three different locations.

Fortunato: "Taxi arrives in 10 minutes.
Guys, are you all right?
You look finished, Sigur."

Sigur: "Everything's okay.
I just didn't sleep well.
I'll get my things."

10 minutes later the taxi honked in front of the door, Lasse and Sigur left the apartment with their backpacks on their backs.
Lasse turned around and waved briefly.
Sigur didn't.

Fortunato let himself fall into an armchair and breathed deeply: "What an excitement!
It's always the same with gringos."
He shook his head.
"First a mate and then check the shop to see what's happened."
He grabbed his tablet and shouted in astonishment: "Hej!
622 Bitcoins!
Since yesterday afternoon.
Whoa!
This thing's running."

He took another deep breath and entered the password for his Bitcoin safe.

In the game room, Edgar took his hand off Sigur's shoulder and left the room wordlessly.


