## On a lake in Tasmania
<div class="_202x">
    <span class="year">2022</span>
    <hr class="hr" />
</div>

<span class="first-words">Marlene was sitting on a jetty on a beach in the north of Tasmania.
She dangled her feet in the water.
She wore a big sun hat.
The evening sun bathed everything in a warm light.
In front of her, a boat tied to the jetty was rocking on the waves.
From the hill behind her two men came towards her.

Hans: "Come! Here, this way ... Fred? Was that Fred?"

Fred nodded.

Hans pointed down the small hill towards the lake where Marlene was sitting.
A trail led right there.

Fred: "That's Marlene there?"

Hans nodded.

Fred: "Can I surprise her alone?
We haven't seen each other for a long time."

"Sure," said Hans.
"Then I'll start dinner.
Today we're going to... let's see...
Casserole with aubergines, zucchini, sweet potatoes and cheese.
All home-grown.
You'll like it."

He turned around and disappeared up the hill.

Fred quietly crept down the path.
He looked at Marlene the whole time and did not want to betray himself with any noise.
When he was three meters behind her he stopped.
She suddenly turned around, but was not frightened as Fred had suspected, but smiled, swallowed, smiled again and held her hand in front of her mouth.

Marlene: "Fred!!!"
She jumped up and fell around his neck.
"Fred!
You're here, you're here!
Super!"
They hugged each other for a long time and then sat down together on the jetty.

Fred: "So, tell me... how are things with you?"

Marlene: "Totally beautiful here.
Hans is so great.
He's lived in this spot for forty years.
He's from Germany.
His father thought in the 70s that an environmental catastrophe was imminent in Europe, ozone hole and all, and moved here with the whole family.
He has a mini-hostel here and together with his wife he grows almost everything they eat themselves.
All organic, of course.
And I'm helping him now, officially.
I'm an exchange student.
I've learned a lot of things: Proper hoeing, like with a real hoe, you know?"
She made a gesture.
"I can stay here as long as I want.
I work here, get food, a bed, internet and a little pocket money.
Many people here in Tasmania do that.
They come from all over the world and spend a few weeks, a few months or a year here."

Fred: "You're not afraid?"

Marlene shook her head.
"Not at all.
I had a good way of getting here.
I got off well from Saudi Arabia as a second wife in a burka on a ship.
A distant relative of Achmed, in the 18th century they have a common great-great-great-great-great-great-grandfather, took me to Indonesia together with his real wife.
There I became a backpacker, with a passport that I still had from Marwin, and then hitchhiked to Tasmania.
Hans here is a distant uncle of a darknet hacker who has an office job in his normal life, nothing at all with computer programming.
They have seen each other once in the last forty years.
But the best of all is Tasmania.
The island is about the same size as Bavaria, same climate, but has only half a million inhabitants, 7 inhabitants per square kilometre.
Everybody knows everybody here.
And not much is happening here.
If anybody is looking for anybody here, everybody knows it immediately.
This kind of thing is exciting for the people here.
So when CIA people show up in the area, I know it before they even get to the house.
And I told Hans that a jealous friend is following me, who must not find out that I am here under any circumstances.
He said, "No problem, he'll tell me everything he hears right away."

Fred shook his head slowly: "Typical Marlene.
You hide in plain sight, where everyone can see you.
That's your thing.
I was thinking big city, Mexico City, skyscraper suburb, anonymous.
But you go where everyone sees you.
Super!"

Marlene: "Hey, and once a week Hans and I go out there with the boat and get oysters.
Fresh oysters with lemon, you know what that tastes like?
Hmmm.
You gotta try this...
But you say now.
I'm so excited, I'm torn.
Did you bring anything?
All you wanted in that last Pond message was my address."

Fred: "I only wanted your address, because I wanted to talk to you directly about everything else."

Marlene looked at him expectantly, "So you got it?"

Fred nodded and pulled a USB stick from his pocket.

Marlene put her hands in front of her face and squealed "Yee-haw!"
She jumped up and danced around Fred, dropped down beside him, kissed him on the cheek and took the stick: "On this?"
She looked at him.

Fred nodded again.

Marlene wrapped her hand around the stick and pressed it against her chest: "How much?"

Fred: "A terabyte, just like you said."

Marlene rolled a tear down her cheeks: "Awesome.
So awesome.
Have you looked at what's inside?"

Fred nodded.

Marlene: "So?"

Fred: "I have already sorted a lot.
A disaster.
Unbelievable.
MacOS has seventeen different backdoors in all current versions.
It's a cinch to get in anywhere with it.
Perfect for script kiddies.
Every student can hack into the biggest companies with the codes and read and delete everything and steal and change everything.
There are things for all operating systems: Windows, Linux, for all programs, for all computers.
They have a separate area for hard drive rear doors, which are already included in the manufacturer's range, with every major manufacturer.
Almost every hard drive that you can buy new comes with a back door that works no matter what operating system you install.
It's a bit like AMT from Intel, only for hard drives and just secret.

<div class="infobox" id="amt"></div>

I'm telling you: The internet is a huge Swiss cheese, and the rats are climbing all over it and have a great life."

Marlene: "Tails?
Goal?
What about them?"

Fred nodded.

Marlene: "Shit.
Bad?"

Fred nodded again. not as bad as Windows and MacOS encryption, but there's something to do..."

Marlene: "Pond, Ricochet, VeraCrypt?"

Fred: "Pond and Ricochet actually seem to be difficult for them, VeraCrypt a few small things, but nothing really critical.
It is clear that the NSA wanted to disable the predecessor TrueCrypt.
It's really good."

Marlene: "Okay.
I've thought a lot about it.
First we send the relevant vulnerabilities to the Tails team, the Tor team, the GPG project, VeraCrypt and so on.
We need secure channels for the other things.
They will then, if necessary, pass on some of the information to the Linux teams.
So, first we close down our software, then we can start initiating larger circles so that they can patch the gaps.
Next then everything in the Linux and BSD world.
It will be a lot of work, but at least everyone knows what to look out for.
If you have a template, it is a hundred times easier to fill a gap.
We need to figure out how much time we're gonna give them.
We can't wait too long."

Fred: "And MacOSX and Windows?
Smartphones?
IOS, android?"

Marlene: "No warning!
They'll see the gaps when we go public.
This is not free software.
They put in a lot of back doors themselves."

Fred: "No warning?
You can't do that.
Most computers and smartphones use these operating systems.
Billions of people.
If you don't give the developers time to fix the holes, then you get chaos, then ... Whew.
No.
For real!
Then every 12-year-old can use a small program at the Federal Intelligence Service and delete files.
Imagine that.
Every ex-boyfriend can look at her smartphone, read everything, delete, change, send messages in her name ... And vice versa.
You can hack all Facebook and WhatsApp accounts, do third party online banking, go shopping with third parties ... And there's plenty of people out there who will."

Marlene shook her head: "I had a lot of time to think about it.
The way they collected the Snowden releases.
Most data are still not published.
And there are hammers with it.
Nope.
No forewarning!
Not a minute.
Only if they first publish a list of all the backdoors they've built in themselves, take them out, and publish the complete source code of their programs."

Fred shook his head: "They never do that!
You have no idea what..."

Marlene interrupted: "We need the chaos.
Otherwise it won't work.
We won't get through if we put the thread in their hands.
Not even a little bit of it.
You hate chaos."

Fred started to say something, but pulled back.

Marlene: "I will tell you what plan I have developed since Saudi Arabia.
It's a risk, but it will work.
But first we need some rest.
We will make enough mistakes, so we need to think calmly and clearly.
The plan must be great.
But we're not starting today.
Today you arrived here, today there is casserole, I guess, today is a beautiful sunset, for the first time with you.
And I don't even know how it went, how you got the stick, how Anita is doing.
Is she still in the NSA?
How did she get the data out?
How did she get the Raspberry Pi into the data center?
And take the stick out again?"

Fred started to say something again, raised his finger, faltered and then laughed: "You're unbelievable!
We've got the biggest thing since Snowden, way more dangerous than Snowden, the collateral murder video, Chelsea Manning's war logs and cablegate put together, and you're thinking casserole."

<div class="infobox" id="collateral-murder"></div>

"Yes," she nodded.
"That's important, too.
And dessert.
Hans makes a great Tiramisu.
Marwin once told me that we will win by having joy in what we do, by not doing things to have joy later, but now.
When we have had our fun with the vulnerability database, we send it away and then others should have fun with it.
Come on, let's go, then I'll show you the garden where the things we're gonna eat are."
