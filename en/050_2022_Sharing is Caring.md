## Sharing is Caring
<div class="_202x">
    <span class="year">2022</span>
    <hr class="hr" />
</div>

<span class="first-words">Marlene and Fred</span> in a room on the top floor of Hans' wooden house.
There were four laptops on the floor and the desk.
Both sat and typed.

Fred: "Good internet here.
How do you cover up the fact that you're using Tor?
It might be noticeable if someone in the middle of nowhere suddenly sends a lot of Tor data to the Internet, and Australia is not exactly a privacy-friendly country.

Marlene: "Quite normal: via Tor Pluggable Transports.
I'm in the process of building my own entrance.
So far I have mostly used the Web or Skype option.
Teenagers surf and skype a lot when they're overseas."

<div class="infobox" id="pluggabletransports"></div>

Fred nodded with an accomplished smile and typed something on his laptop.

Fred: Okay!
Data is ready.
I'm ready.
I can now look up which vulnerability exists for which computer or operating system.
Who comes first?"

Marlene: "As I said: Tor, Tails, Cubes OS, GPG, the OMEMO projects and VeraCrypt."

<div class="infobox" id="gpg-omemo"></div>

"We send the weaknesses of all their projects to them.
I've already warned the core people:
There's an earthquake coming, and a flood.
They know exactly what happens if the information goes to the wrong people right now.
And they know how to stop it."

Fred: "Are we sending everything?"

Marlene nodded: "Yes of course, everything about her projects.
Everything from the database you made.
It is important that there are no more gaps in the programs before things get really big.
We have to be able to communicate."

Fred: "How much time do we give them?"

Marlene: "Let them say that themselves.
Three days, I guess."

Fred: "THREE DAYS?!
Are you crazy?
How can they fill the gaps in three days?"

Marlene: "With the six projects, there are not so many.
They know exactly what to look for.
They usually react immediately in case of security gaps, they are highly active in their projects.
But we ask them.
They won't say much more than three days.
I'm still writing that we're on a tight schedule."

Fred: "And after that?
I have an info pack here for each of the different Linux variants, for BSD, one for Mozilla, especially Firefox and Thunderbird, KeePassX and a few others.

<div class="infobox" id="thunderbird-etc"></div>

Marlene: "Yes, that sounds good.
Meanwhile I have lots of current Ricochet addresses of all kinds of teams.
We will send them as soon as the vulnerabilities in the base programs are gone and we all have the new versions.
These teams need more time.
That's a lot of stuff.
And then let's tackle all the other Linux, Libre Office and the hardware things."

Fred: "A lot of work."

Marlene: "Yes."

She types on her laptop.
"Okay... so... news to the first six teams is out.
I think we'll get a quick answer.
Tell me, what was it like with Anita and the terabyte?"

Fred: "Yes!
Anita and the terabyte.
It's a story.
So, after meeting everyone where you were in the Boatly Hacker space:
I get your Pond message, which you also sent to Anita, and fall off my chair for now.
I hadn't been quite serious about the Raspberry Pi in the chat: rather so ... you could do.
Then I pick myself up again, take a breath, sit up straight and write the Raspberry program.
Not so difficult.
Three quarters of an hour.
Then I test it with a few thousand QR codes, it's okay, make an image of it and send it to Anita via Pond.
No response, just a normal pond acknowledgement.
She's got it.
Good.
Next, 22 hours 10 minutes later, I get a message that the files are safely stored on two 1TB sticks, asking how they should be delivered.
I fall out of my chair for the second time, crawl out again, sit upright again and continue reading.
She says she may be followed by NSA people.
She's not safe, but if they have any indication, she's being monitored.
Her suggestion is that she gives a stick in a rest area to the toilet attendant there, who has been sitting there for at least 10 or 15 years.
She gives her 50 dollars and says that a man will come to her home who will give her another 50 dollars for the stick.
She gave me the wife's home address and said that she usually arrives at home at 6:20 pm.
Well, Anita must know.
She's in the NSA.

Marlene: "Shit.
Has NSA surveillance helped us with the operation now?"

Fred: "Anyway...
So I'm going there at 6:30.
She opens the door, looks at me from top to bottom, turns around and gets the stick.
I put $50 in front of her and she says, "$100!
I close my eyes, wait two seconds and then take another $50 bill out of my pocket.
In all this, she makes a face as if she had drowned a group of young poodles just for fun.
She takes the two bills and gives me the stick.
I have him in the palm of my hand.
My hand is starting to tingle.
I get back into my car and have to breathe deeply and sit upright again.
And then I'm going straight home.
It was."

Marlene laughed: "Horny.
Nice.
A lavatory attendant."

"Pling," it came from Marlene's laptop.
"Arma replied: Three days!
I told you.
They're great guys!
It's so good that they exist."

Fred: "And girls.
Well, they're all working now.
There's no lights out for three days now.
You bet."

Marlene: "And they are happy like snow kings ... finally some real NSA data."

Fred: "They dance in circles..."

Marlene: "Christmas and birthday in one day."

"Pling," it came from Marlene's laptop again.

Marlene: "And Werner Koch too.
Also three days.
Look: a little heart from Werner."
She moved her laptop to Fred.
Marlene: "He's not usually this emotional."

Fred: "Wow!
A heart from Werner..."
