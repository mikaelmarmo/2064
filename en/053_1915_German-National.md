## German-National!

<div class="_19xx">
    <span class="year">1913</span>
    <hr class="hr" />
</div>

### SCENE 7
____
A group of officers in a beer tavern at Tegel harbour. A lot of smoke, loud conversations everywhere.

First officer: "Wilhelm and Hilde Papenburg, they sully the honour of the German people.
They want to fall into our arms when we take up arms against our bloodthirsty enemy in order to regain dominance over Europe.
They want to weaken our strength, they make communion with our enemies: with the French, with the English, with the Russians.
They are the first enemies on our path to making Germany what it is in essence: a leading power.

Second officer: "Yes, sir!"

First officer: "And we must learn that a new era has dawned: the machine era.
On the battlefields the ones with the better machines will win.
Armored vehicles, armored aircraft, machine guns.
But also the machines that produce machines, also the machines that cultivate land, build roads and houses.
Our industry is the decisive factor on our way to the top of the world.
And the great people who run the companies: They are leaders of labor units just as we are leaders of army units."

Second officer: "Hurray for the German economy."

Third officer: "The strongest in the world!"

Fourth officer: "After the Americans.
Still... We've overtaken the English this year.
America is the great danger.
He who leads the industry leads the world.
We need powerful, growing and agile companies.
Our companies already have workers working for them around the world.
We do not have to colonize countries, that is yesterday's news.
We have to open up countries for us so that our companies can have the workers there working for them.
And the people there buy our goods.
And because we are stronger, we will win in every country that opens up to us.
It's the new race.
Not colonization.
And that's where America is one step ahead."

Third officer: "They build the tallest buildings in the world.
The longest railway lines."

First officer: "We will let the weapons speak, we will shift the world order, we will give our companies space to spread everywhere.
And for that we must go to the battlefield.
For that we need respect.
We get that there.
And that is why, that is why I have invited you here today to talk to you about what we can do against those who want to disturb this path from within."

Second officer: "Wilhelm and Hilde Papenburg..."

First Officer: "...and their corrupt movement.
The Spartacists.
And others too.
But her first.
They poison the hearts of the German workers.
They no longer want to work diligently for the cause, they want to work less, have a say, they want to change rules.
They want to own the companies where they work.
Everywhere I look, I see betrayal.
Treason against the German cause.
Treason against the German people.
To the German entrepreneurs.
The German people should be strong, not weak.
It should prevail against the other countries, not bow down.
The socialist utopia will drive us into slavery.
America, England, France, Russia, they respect strength, not weakness.
Also our friends: Austria and Italy, respect the strong, not the weak."

Third officer: "We can eliminate them.
I mean permanently.
No more warnings, no more intimidation.
You must leave before the disease spreads to other working classes."

Second officer: "Yes, sir!"

Fourth officer: "I have them watched around the clock.
We have some of our people in their movement.
There will always be someone around.
And then we'll come up with a plan how to do it."

First officer loud: "One for all."

All together: "And all for one!"
____
