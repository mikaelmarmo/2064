## Intimidate!
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Marlene and Lilly</span> sat together with a cup of tea in the stone hut.
A campfire was burning before them.

Lilly: "Did that really happen with the cyanide capsule?"

Marlene: "I did not experience that myself.
When Ali Steffens and Eduardo Lampresa cracked the LBI Bitcoin safe, I was just in prison.
I didn't catch any of that directly.
But Ali Steffens had something in his body that he had to watch out for.
Unfortunately he didn't get a chance to write his story.
But it may well be, it was already very brutal back then.
But I myself never had anything in my body.
Just a laser tattoo on the forehead that you can read from satellites.

What was for sure were real torture centres where many people died.
First in East Africa and the Middle East, but later more and more in the central cities like Berlin, Moscow, San Francisco, Quito and Paris.
They were mainly there to intimidate.
It wasn't so much about punishing people or getting information, it was about the message: keep calm and nothing will happen to you.
And if you don't shut up, it's gonna be bad...

And it worked.
Many bent over, many were afraid, some panicked, even hackers.
But not all of them.
Some hackers can't bend.
Actually, you stop being a hacker when you bend.
And the secret services have said that everyone will bow down if they show their superiority just enough.
You were wrong."

Lilly: "Like your brother..."

Marlene nodded: "Yes, just like my brother.
Uh-huh."
She shook her head slightly.
"That was unfair.
... I still get a shiver down my spine thinking about him.
He did not bend.
He's got her pegged.
He could talk to them.
I was so fond of him.
And he had to die so soon."

Lilly: "Yes, that was the beginning for you, right?
Somehow.
This is how I understand your book."

Marlene: "Yes, that was a beginning.
That was where it was only serious for me, for a while.
Before that I played a lot.
I simply did what I thought was right.
Also the NSA database.
After that it is more difficult and darker.
That's why it hit me once.
And ever since then, I've been limping a little when I walk."

Lilly looked at Marlene's leg in surprise: "That's why?"

Marlene: "That was in prison.
They are really brutes, not human, at least when they are soldiers.
They may be people in their personal lives.
That's really schizophrenic.
Absolutely icy and brutal at work and soft and nice at home with partner and children.
This was also the case for many people in the companies at that time.

But even that can be understood.
If you know her story.
I got to know one soldier better in prison.
Pretty much.
That was a turning point for me.
His story blew my mind.
That's when I learned how important it is never to be against anyone, but always to keep your own path in mind and try to understand the other person as well as possible.
Even if he wants to kill you.
Marwin could.
He could always look below the surface.
He knew what was going on, just like that.
And he told me over and over again.
But I didn't understand right away.
It's in the 11 basic ideas Marwin wrote down in prison.
I read them, over and over again, but I only really understood them when I was in prison myself.
The same as him, by the way.
It is perhaps ironic that I ended up there.
Or it was simply again intimidation.
There's so much about intimidation, they want to look all-powerful.

Ali and Eduardo were great.
There was no way to intimidate her.
They had just somehow decided not to be intimidated and they went through with it.
And now Lasse and Sigur are re-enacting it.
Who knows, maybe they will manage the whole mission and then there will be a meeting between Ali and Eduardo and Lasse and Sigur.
Unfortunately only as holograms of course.
But they're really good.
We've been polishing this for a long time.
It'll be exciting."

Lilly: "The mission was so awesome.
I had TRON show it to me.
And Fortunato?
Do you know anything about him?"

Marlene: "Fortunato was actually called that.
I even met him once.
But it wasn't much of an encounter.
He's nice.
Funny.
And always at least two women around him.
But I couldn't talk to him very well.
He's just like in the game.
A super likable asshole."

Lilly: "What will happen to the Bitcoins?
Are these two on the right track?"

Marlene: "Absolutely.
And what happens next?
Hmm.
It depends on what they're doing now.
If they find what Eduardo did.
Or something like that."

She smiled at Lilly.
