
<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>
<div class="title-large">Part 3</div>
<div style="visibility: hidden;">\pagebreak</div>

## A drop of blood
<div class="_202y">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Marwin looked at the door</span> and thought:
"Still no guards... It's been at least half an hour since Linda fled the room.
Yeah, she escaped.
But from what?
Surely before saying anything about Marlene...
Shit.
Shit.
Shit...
What about Marlene?
Linda knows where she is...
Yeah.
She doesn't want to find them.
She doesn't need me to find her because she knows where she is.
But then what does she want?
Why release me?
None of it makes sense."

"Damn it," he said out loud.
"Fucking hell!"

He laid his head on the table.
His nose started bleeding.
He crumpled up a piece of handkerchief and pressed it into his nostril.

"Marlene is forcing her to let me go," he thought and smiled, "Yes, that would suit her."
He nodded.
"She has no boundaries.
That's a nice thought, but, no.
You won't be blackmailed.
You can't.
On principle, no.
They are the greatest world power, the greatest country there is.
At all times.
They are all part of a miraculous dream, the American dream, the country where everyone can make it to the top.
No, there can't be an 18-year-old girl coming in here making them do anything."

He buried his head under his hands.
"Or was it really a change of heart?
Linda had hinted at something: They want to dismantle the NSA...
No, that's bullshit.
Even less likely.
Only if they have something better than the NSA and they don't need it anymore.
Certainly not because they're reflecting on their ideals: To freedom, self-development, letting the other person live.

Or are they beginning to take their own ideals seriously after all?
To take yourself seriously?
To apply the ideals not only to their country, not only to a part of the people, but to all humanity?

No.
Their goal is to subdue the world.
It's the only thing that makes sense to them in a crazy world.
Order through power.
The strong can use his goodness, the weak cannot.
They get that with breast milk:
If the world is to go well, we have to control it, with our armies, with our financial industry, with our arms industry, with the Internet industry, with fast food, Coca Cola and with the mass media.
Controlling every aspect of life.
That's the goal.
And when you have achieved that, then you can guarantee the freedom of people - at least from that part of people who are positively disposed towards them.
Maximum happiness for the maximum number of people."

He hit his fist on the table: "Fuck".
He thought: "This is the most demonic saying there is, because it sounds reasonable and is so hard to understand.
If you can exclude people, then the greatest country that has ever existed excludes all those who do not submit to its culture, its way of life.
Everyone who wants to live differently is an enemy of freedom, a danger.
For example, environmentalists, human rights activists, hackers, especially cypher punks."

He clasped both hands to his head.
"By force to harmony...
With oppression to freedom.
What makes people think such idiotic things?
What makes hundreds of millions of people, in governments, in state apparatuses, in companies, think such nonsense?
How can they think that power can solve problems, concentration of power, in our time?
Power must be in the hands of the people who actually do it: program, build, plant, clean ...
Power must be distributed like water over a watering can.
Not unfolding like a reservoir breach."

He was breathing heavily.

"And because of this error of reasoning, I have been sitting here for two years now, completely pointlessly, without accusation, not even a reproach, not even a false accusation.
If only ten, twenty percent of the population were real hackers, cypherpunks, people with cypherpunk hearts, things would be different."

He looked around the room and got stuck on the table on a small booklet and pen that Linda had forgotten there.
He looked at her and then suddenly shouted, "Yah!
Right!
A manifesto!
Like Eric Hughes in 1993.
I'm writing a hacker manifesto, a new cypherpunk manifesto.
This is it.
And if no one ever reads it..."
He pulled the booklet towards him, tore out all the pages already described and arranged them in a stack on the table.
Then he opened the first page and took the pen.
He thought, "Eric Hughes. encryption" and began to write:

<div class="thought">
It all starts with encryption.
</div>
He looked at the first sentence, nodded and continued writing.

<div class="thought">
1 -- Encryption, like mathematics, cannot be destroyed, not by armies, not by intelligence agencies and not by the most powerful corporations. It is the foundation of our personal freedom. We must be able to choose who gets our information and who doesn't. We must be able to hide, to avoid the eyes of those who want to destroy what we create. Every beginning needs protection.
<br /><br />
And we need free software that cannot be manipulated because it is protected from modification by encryption, and everyone,
can trace every change to it.
<br /><br />
The universe gave us the encryption.
</div>
A drop of blood fell from his nose onto the paper.
It looked at him with fascination and made sure that he did not run, but dried as he was.
