## Bomb threat
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Lasse sat in</span> a restaurant on the 10th floor.
Floor of a shopping center in San Diego, USA, with a magnificent view over the ocean.
Across from him sat two men in black suits.
The atmosphere was heated, both sides tried to appear relaxed.

Lasse: "I have four Bitcoin bombs."
He pointed four fingers.
"And starting tomorrow, one will go off every day at 8:31.
Then your Bitcoins are distributed in a fraction of a second over thousands of Bitcoin Wallets.
Over thirty billion dollars."

First man: "This is no problem for us.
We can prevent that.
For a short period of time, we will be provided with all data centers and hackers of the TAO.
We'll just download all the servers that can handle the transactions in that time.

Lasse laughed out loud.
"All 700,000?
Quit joking!
And even if they did, it would only delay everything for a few minutes, half an hour at most.
Most servers have an auto backup:
They automatically reappear somewhere else on the Internet and continue where they left off last.
Some even open several servers at once: like the Hydra in ancient Greece.
You cut off one head and three new ones grow.
They all learned from WikiLeaks and Piratebay, and you never really got a handle on them.
Maybe a couple of days.
And now you want to cripple Bitcoin altogether.
Come on, guys, seriously..."

First man: "Different scenario.
The bombs have exploded.
The money is gone.
And then what? ...
Then what? ...
If you've blown all the Bitcoins and you've got nothing we want, nothing at all...
Then what?
Then what do you do?"

Lasse: "I'm trying to get out of here.
And if I don't make it, you'll kill me."

First man: "First Sigur gets it.
And for you, we have a bunch of agents here in San Diego.
Escape is not an option."

Lasse looked him in the eye: "Okay.
Then you kill me...
And then what?"

Second man laughed: "Then what?
Then you are dead!"

Lasse turned to the second man, "Yes, fine, and then what?"

He turned slightly red in the face, reached into his jacket and pulled out a pistol far enough for Lasse to see it.

Second man cold: "We might as well do it now.
It is silenced and has uranium ammunition.
A graze would do it.
I always shoot three times.
First twice in the heart and then in the face.
Your corpse is going into the sea, 5,000 meters deep.
And we'll give you a good story on how the rest of the world will see you.
For example: Lasse Brodowin, the defector, submerged, in reality leads a luxurious life.
We won't let you die for your friends.
To them, you'll be just another traitor."

Lasse: "But then the Bitcoins are gone...
that is not an option for you."

The first man pushed the first man's gun back into his suit pocket.

He turned to Lasse: "We are here for an agreement that suits both, which both sides can live with.
So, once again: Sigur gets free.
Where do we go from here?
How do we get the Bitcoins?
How can we be sure we'll get them?
How can we be sure you have them?
We need a guarantee.
"The Bitcoins must be deposited somewhere before Sigur's release, we must be able to verify that they are there."

Lasse shook his head.
"It's not gonna happen.
I wait until I get the signal from Sigur that he is free.
Then I'll send the Bitcoins.
I don't make any guarantees before."

First man: "This isn't working."

Second man with red head to the first: "We might as well take him with us.
The night is long until 8:31 tomorrow morning.
If I may, he'll tell us how to get the Bitcoins before midnight.
And I don't do pill-picking like waterboarding or standing naked in a cold, dirty corner.
It's quicker with me.
You can see some of it afterwards."

He kicked Lasse firmly under the table with his boot.

Lasse touched his leg: "Ahhh.
Asshole!
Shit!
Why are there so many assholes in your country?"

First man cool: "He had a difficult childhood.
His mother beat him to the hospital a few times.
She was a drug addict and manic-depressive.
Single parent.
That's what shaped him."

Lasse and he looked each other in the eyes for a while.

First man: "It will not go as you say.
We need security."

Lasse: "I can now make sure with the push of a button here that I no longer have any possibility of stopping the bombs.
No one can do it anymore."

First man: "Half the TAO is looking for the computers that can trigger these Bitcoin transactions.
They're somewhere on the Gate Network.
Maybe we can find her."

Lasse: "You know that you can't find them there.
700,000 computers, gate computers.
Nope.
You had a lot of problems with 4000, 5000 in the past, so 2010, 2011.
Besides, you need the Tor net itself for all kinds of things.
You don't have anything of your own that's that useful.
You can't just turn it off."

They looked at each other again for a while in silence, eye to eye.
Lasse with a quiet view.

First man: "You have to give me something.
When I come back with the result, then I am away from the assignment and then they give you alone in the hands of him here, for example.
Even after the Bitcoins are gone.
They do it for revenge, they're not good losers...
they call it prevention...
Come on!
Again, we'll see the Bitcoins beforehand..."

The second man bit his teeth and breathed deeply, insistently, hissing: "I have special scissors.
I'll cut off your index finger with this first.
Without having to expend any energy.
Off.
Actually normal household scissors, only shortened and specially sharpened.
And then the ring finger.
Off.
Then the small one.
Let's see if you still give us the middle finger."

Lasse looked over the ocean.
He closed his eyes and exhaled.
"Don't go against it," he thought.
"Or else it's over.
Otherwise I don't stand a chance...
What an asshole.
Such assholes.
What happened to them later?
Where are they in our time?
What have they done with their lives?"

In his trouser pocket he clasped the push button that would change the passwords for the bombs somewhere in the Tor network.
A long pressure and he could not get to her, and no one else could.

He leaned back and looked at the first man: "Then I'm going to press the button now!!"

"No! Stop!" they both cried at about the same time.

Lasse shook his head and looked at the sea again.
On the horizon, far out, hovered a huge white balloon.
Lasse looked at him dreamily.

