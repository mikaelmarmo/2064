## Crisis meeting
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Heinz and Frederik</span> sat in casual clothes at a large, fully occupied conference table in the BND headquarters. Some had suits on, others were dressed casually.
Four participants sat on chairs against a wall, the president of the BND sat at the head end.

President: "Okay, what do we know?"

A man in a grey suit stood up and went to the projector, which threw a picture of Marlene and some personal data on the wall:
"Marlene Farras, 18 years old, Eckener High School here in Mariendorf until two years ago, until then nine months under intensive surveillance by us, from then on worldwide search, initiated by the CIA.
Turned up briefly in Dahab, Egypt two years ago, then probably Saudi Arabia, last year probably Tasmania ..."

President: "Probably?
Probably?
Is this another intelligence operation?
Or is this "Who Wants to be a Millionaire?
Do you need an audience joker?
Marlene Farras has been in our focus for almost three years, and we have a crumb of nothing.
And even this is from the Americans."

Man: "Tasmania is ours.
We have an arrest record that says so.
A Darknet hacker."

President: "Who is it?"

Mann: "I can't say, not everyone in this room has the necessary security level.
I think he's gonna turn state's evidence.
We'll have him soon enough."
He scribbled something on a piece of paper and slipped it to the president.

The President read it: "Okay.
I see...
So, she's had no contact with family?
To friends?
No usual places?
Hackerspaces?"

Frederik: "Negative.
We check everything regularly.
Hackerspaces we can not say for sure.
There are too many of them. They're all heading for stations...
And because of the mess last year, we had lost track of things at times.
Smartphones were often unavailable, Facebook, WhatsApp accounts were closed, there is still much more encrypted communication, much more is bought with cash and Bitcoins.
But in the meantime we have complete access again to family, class, friends we know.
In the hacker environment it is traditionally harder: Pond is spreading, Ricochet, GPG is very strong, more and more used on the command line, Tor, OMEMO ... The usual suspects."

Another man: "Since last week there are new leaks again!
iPhone could become more difficult again in the near future.

Frederik: "Android as well.
Whistleblowers are active in both companies.
Still!
They can't handle it there.
There are probably 15, 16 whistleblowers at Apple, Google, Microsoft, Facebook and Yahoo alone.
Keeping them away from the public is hard work.
We intercepted some stuff before this happened last week.
Our media people do almost nothing else..."

President: "What channels do the pipes, uh, whistleblowers use?"

Frederik smiled tortured for a moment, and continued: "SecureDrop, GlobalLeaks, WikiLeaks, own stuff ... It gets confusing.
You can open up a pretty good drop with a little Linux know-how.
Even Spiegel Online has one."

President: "We must be careful.
There is still a lot of attention to the topic in the population.
The Chancellor is worried.
She asked me about it personally.
This warrant comes at a bad time.
With this the attention rises once again by leaps and bounds.
We were supposed to take it down this year.
People are beginning to attach importance to the matter over a longer period of time.
Something can then remain there, as with the climate movement.
The arrest warrant may work in America, but here in Germany it is a disaster.
Any suggestions?"

Heinz: "Marlene's brother is sitting in a CIA prison in the USA.
Maybe we can use him."

President: "How?"

Heinz: "Release, pursue.
He'll certainly be looking for her."

Frederik laughed: "But you have to do it really well.
Marwin Farras is not just anyone.
He smells a fuse.
Don't underestimate him.
He was in our system for months, and we didn't detect him."

President: "But now he was with the Americans for almost two years.
CIA prisons like this are not recreation homes.
That shapes.
Maybe he's changed..."

Frederik: "Okay.
I'll give them a call."

Heinz: "And we are taking an even closer look at their school class.
We take the next hop into intensive surveillance: their friends and relatives, as well as their family.
In the hacker environment, we cannot do more in terms of effort than we can now.
That's where we're at."

Man: "And I'm gonna put a little more pressure on our darknet hacker."

President: "Okay, let's get to work!
That this will be a real intelligence operation again."

