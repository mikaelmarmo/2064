## Interrogation (1)

<div class="_19xx">
    <span class="year">1918</span>
    <hr class="hr" />
</div>

### SCENE 8

A dirty sidewalk cafe in a shopping street somewhere in Addis Ababa, Ethiopia. Marlene was sitting in a corner, half behind a column, drinking a tea.
Two local young men with black sunglasses looked over at her from a neighbouring table.
She looked away, reached into her rucksack and took out a paperback.
She was about to open it when suddenly a flatbed truck stopped and men in uniform and sunglasses jumped down.
Three of them stood in front of the cafe with machine pistols at gunpoint.
On the other side of the street something similar seemed to happen.

Marlene.
She watched exactly what happened next, put one hand on her backpack.

"No problem!" cried one of the young men from the next table.
"It's only the Home Secretary, he wants to have a coffee over there."

She nodded at him, picked up the book and opened it.
In the middle of the book, she had cut a hole in it, into which a Raspberry Pi with screen fitted exactly.
She tapped the screen, left some messages and closed it again.

Then she closed her eyes and leaned back.


/// BLENDE /// 1918, Hilde.

____
Hilde, tied to a chair in a large vaulted cellar.
She has a gag in her mouth.
Isolated flickering lights on the wall.
Around them a group of differently uniformed young men.
A captain.

A man of the group takes two steps towards Hilde and hits her in the face with his hand.
She tries to dodge, but the blow hits her so hard anyway that she tips the chair to the side and falls to the floor.
She bangs her head on the floor and stays there.
Your lip is bleeding.
Her face is dirty from the damp floor.

Captain: "Pick it up!"

Two of the men grab her and straighten her up.
The captain takes a chair and sits on it upside down in front of her.
He puts both arms on the back of the chair and grins at her.

Captain: "Harlot, we warned you!
You messed with us.
Now you'll get the bill for this."

Hilde stammers something through the gag.

Captain: "Quiet!
It's not your turn now.
It's my turn now: we warned you, several times.
You just kept going.
None of this would have been necessary if you'd backed off when you had reason.
But you leave us no choice."

Hilde is stammering again.

Captain: "If we were now to run towards the French with open arms, as you Spartacists want, we would all get a bayonet or a bullet in the chest.
And that way you all want to hug: English, Americans, Russians, Chinese, Negroes, the whole world.
It's pure stupidity to trust everyone, madness.
It's suicide.
We can only rely on our own strength and nothing else.
The weak have no friends.
The weak become slaves."

He looks into her eyes with a vivid expression.

"You help the weak.
You've always done that.
Even back in school.
You helped those who couldn't get anything done because they didn't want to work.
And you still do.
Give them food, a place to sleep.
Take away the last reason they would ever work.
They prefer to steal only from those who work hard and honestly.
And you help them.
The German nature works hard and is honest.
That's why everybody buys our goods, all over the world.
"Made in Germany", that has sound.
This is the future.
We are better than everyone else, we can work better, we can organise better.
We have the best inventors.
When we mix our culture with others, we weaken them.
Cultures cannot live side by side on equal terms.
There must be a guiding principle, a guiding culture.
And it's German."

He takes out his gun, looks at it.
Cradle her in your hand.
Then he suddenly holds it to her forehead and pulls the trigger:
"We need a German guiding culture.
We must remove the inferior elements from our society.
Wouldn't you agree?
I bet you feel the same way.
We've prepared a statement for you to sign.
Are you ready?"
She's babbling something incomprehensible.
"Ahh? Yeah? I see!
You can't talk.
BERTHOLD!"

A man approaches from behind and opens the gag.
Hilde is coughing.
The captain takes his pistol from her forehead and looks at her: "Well, are you ready for this, Hilde?"

Hilda in a rough voice: "Never!"

The captain grins: "I was kind of hoping you would say that.
I've always hated your whitewashing of scum.
It has its appeal, killing someone.
Especially when it's an assignment, when it's mandatory.
That's quite an experience.
Not many can kill.
It will be my pleasure.
And I even get a badge for it."
He aims his gun at Hilde's forehead again:
"Last chance, bitch.
Ah.
Berthold, the tub.
I don't want it to be that messy again."
Berthold comes back and pushes a metal tub behind Hilde's chair with his foot.

Hilde looks seriously at the captain: "What you do, you are responsible for.
You personally.
You're an adult.
You cannot hide behind a command or any group or leader.
What you do, you do yourself."

The captain laughs: "Yes, I will.
Just me.
With complete conviction."
He smiles.
"Together with Berthold."
He presses the pistol harder against Hilda's forehead and shouts slightly excitedly: "Think your last thought!"
Then he pulls the trigger.
Her face freezes, her neck twitches.
"Click!"

The captain laughs: "BOOM! You're still alive.
Did you think I was gonna shoot you here so easily?
No, no.
We still need you.
You know some things we want to know.
You're not getting out of here that easy."

Hilde closes her eyes and breathes hastily.
She has gone completely pale.

Captain: "First of all, I want all the names of the Spartacist committee from you, and I want them now.
I'll tell you one: Wilhelm Papenburg.
We already have.
Now I need nine more.
Three of them we know for sure.
If they're not on your list, I'll shoot you in the foot, once for every one missing."
He turned round.
"Berthold, have you got the bandages?"

Berthold: "Yes! Here."

Captain: "Or, hmm, I'll tell one of the boys here to give you a real hard time, like a man.
You don't look so bad yourself.
Well?
How's that?
Are there any volunteers?"

From the group: "Yes!", "Yes!", "Here!"

Captain comes close to her face: "Well?
Which would you prefer?
One of them or your foot?"
Hilde spits a thick chunk of saliva in his face.
The captain straightens up, trembles and wipes his face.
He takes a deep breath and then strikes Hilde in the face with his hand.

Captain: "You bitch! We can also be different! Bitch!"
He walks around the room excited.
"Slut! Berthold!!"

Berthold: "Jawoll!"

Captain: "Bring him in.
We'll move on to step two."

Berthold: "Jawoll, Herr Hauptmann!"

He bends down to Hilde, goes with his mouth very close to her ear and whispers in a low voice: "Well? You know what's coming now, don't you?
Don't you want to spare yourself this? And Wilhelm too?
I'll tell you the price: you write down the names.
If the names are right, you can go home and cook yourself a plate of noodles.
Maybe take a shower first...
Hmm? How's that? ..."

Hilde shows no emotion.

"Klock!", the door jumps open and two of the soldiers drag a man in, who is struggling powerlessly with his legs.
They put him on a chair about ten metres from Hilde, so that she can see him just out of the corner of her eye.
He can barely stand up straight in the chair.

Hilde looks over and thinks: "Oh no. Wilhelm..."
____

/// BLENDE /// 2023, Marlene.

Marlene opened her eyes and moaned slightly, "Fuck!"

"Pling" came softly from the book.
She opened it, let it go and smiled.

Marlene: "Lake Maggiore? Italy? Yah. "Why not..."

She put the book back in her backpack, put money on the table and set off.
