## The Manifesto (1)
<div class="_202y">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Marwin ran up and down the meeting room with</span> a pen in his mouth.
Suddenly he took a big step towards the wall, stopped in front of it and bounced on his feet: "Encryption, encryption, encryption... Hmm.
And then... Yes!" He snapped his fingers.
"Yes, exactly!" He sat down at the table, turned the pages and wrote:

<div class="thought">
"2 -- The fundamental ideas of our time, economic growth, money gain and security, have no more power.
We cannot carry them with enthusiasm in our hearts.
They create everywhere opposition, distrust and fear.
<br /><br />
We need new ideas, lively ideas, ideas that can unhinge everything that is old and crusted.
And we already know these ideas, we already carry them within us, but they do not yet determine our culture.
For example: having confidence in people, courage, all seven kinds of courage, creative cooperation, protecting the environment, a transparent state, transparent companies, overcoming oppression ..."
</div>

"Oppression, against each other..." he breathed in and turned the page.

<div class="thought">
"3 -- We must overcome the opposition. There is only one humanity. All people belong to it. Not a single one must be missing.
<br /><br />
Being against each other separates what is actually connected. We have a world economy, but not the same world heart and world thinking. There are no terrorists, no anarchists, no Nazis, no politicians, investment bankers or soldiers. There are only people who are blinded by the opposition, who kill, rape, intimidate, abuse, oppress, manipulate or set Shitstorms in motion, because they themselves are afraid, because they are blind ... with fear. Fear can be cured - but always only your own fear."
</div>
He turned the page.

<div class="thought">
"4 -- We must keep sacred the initiative, the impulses in each person, including those in ourselves. From our own initiative comes our strength to live. We need them like food and drink.
<br /><br />
We can create spaces where we can meet each other, meet others, invent things together, be creative, follow our own impulses freely. We must learn where the boundaries to the other are, at what point I violate an impulse of the other, and learn to hold back and let him ... ...to let him live."
</div>

Marwin: "Let him live. and understand how he thinks."
Marwin was thinking.
Then turned the page and continued writing:

<div class="thought">
"5--We don't listen to each other.
We hear the words that someone else says and make our own thoughts from them.
We don't have what the other guy thought.
What the other person thought, he put into words: first filter.
Then we have heard the words, transform that into what we understand by the words: Second filter.
Then we pick out one or more thoughts from these twice reshaped words: Third filter.
Then we no longer have what the other person thought.
Not what he said anymore.
Not even what we heard.
We only have what we think about what we've heard.
Everything we talk to each other goes through these three filters.
Think - say, say - hear, hear - think.
And often my thinking differs from the thinking of the other in a grotesque way, sometimes in things that decide about life and death.
Wars are fought, murders are committed, people are excluded - just because we misunderstand each other.
We must understand what our words mean to us and to others if we are to talk to others.
We can run a retest: Repeat the other person's thoughts.
Then the other person can say whether it was what he meant.
<br /><br />
We have to stop judging what anyone else says.
What he says is not meant to be evaluated as right or wrong, but only to understand what he thinks.
And the better we can transform our thoughts into words, the better the other person can understand us ... and add new thoughts to ours."
</div>

He put the pen on the table and stood up and said aloud: "New thoughts ... go on ... ...let the other guy surprise you."

He turned to the mirror wall, behind which probably people were sitting and watching him now.

Marwin: "And you? What do you think?"
