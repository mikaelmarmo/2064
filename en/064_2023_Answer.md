## Answer
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Kevin looked at</span> the clock
Half an hour had passed since they had sent the message to WikiLeaks.
Still no tweet, no response, no reaction at all.

Lukas: "You'll write, Kevin.
Don't worry about it.
Otherwise we'll find another way."

Kevin was a little insecure: "Show me the video again."

Luke slipped him Sophie's laptop.
There was a knock at the door.
Anni got up and opened the door.
"No teacher would knock," she thought and smiled.

There was a man outside about 25 years old.

Man: "May I come in?"

Anni: "Yes, please."

Luke looked at the door and shouted: "Oskar!
What are you doing here?"

Oskar laughed, "Luke, you're here?
In _the_ class.
I didn't know that.
That's good!"

He walked up to him and they hugged.

Luke: "Hey, it's no coincidence that you're here, right?"

Oskar shook his head: "No.
I come from WikiLeaks.
I was just the closest to you, so they asked me to stop by.
You need information."

Lucas: "WikiLeaks!"

He turned around and called out to the class, "Hey, Oskar here is from WikiLeaks."

Everyone looked up.
Kevin and Anni stood around Lukas.
Others were added.

Luke: "What kind of information do you need?"

Oskar: "Did you just send this message with video to WikiLeaks?"
He held a strangely clunky smartphone against Lukas.

Luke took it, looked at it and nodded, "Yes, we did."

Oskar clenched a fist: "Cool!
Then this is real!
Great!
Hey, what you are doing is really good!
It's explosive, I can tell already.
At WikiLeaks, some people are super excited about a confirmation at the moment, you can believe it.
It's all they're doing right now.
This is gonna be huge.

So if you want a retweet, send the codeword: "Marlene for President" to WikiLeaks.
And if you want them to not only retweet, but do whatever they want with the letter, send "Marlene for President!"
Then they'll really spread the word."

In the meantime the whole class had gathered around the group.
Kevin sat down in front of his laptop and started typing. "Marlene for President Exclamation Point," he read aloud.
"Anyone object?" He looked round.
"No?
- Okay.
She's gone!
Yeah!"

He jumped up and clapped his hands with Lukas, then with some hesitation also with Oskar.
Other students did the same, many hugged each other.

Kevin was jumping around: "Horny, horny, horny."
He jumped up to Sophie and hugged her.

Oskar laughed and looked at Kevin, then at the class: "Hey, it's cool here.
I think I'll stay here for a while.
That feels good.
Maybe I can help you a little bit, too.
What's the status of Opsec on your end?"

"Opsec?" asked Lukas.

Oscar: "Operational Security.
How well are you secured against surveillance?
Do you have Linux?
Cubes OS?
AMT-free computers?
A tin can for all Smartphones?
I mean, this is gonna be an intelligence op.
They want to be here live, you can be sure of that.
But we'll get it done.
We'll let them swear a little.
For example with something like this," he held up his very clunky smartphone.

Luke: "What is this?"

Oskar: "A Raspberry Pi with a touch screen.
Completely without Android, without a modem chip, a normal Linux computer that you can put in your pocket.
If the Raspberry Pi is freshly bought, has never been on the internet and then gets tails, it's a pretty tough nut to crack for the NSA".

Luke smiled, "I want one of those."

Oscar: "60 €.
I think we have a few more lying around...
I'll bring you one next time."

Oskar looked around the classroom: "We could use a tension hammock up there.
I'll bring one back from hacker space.
And we need good internet too, lots of good internet..."
