## A flying combat robot
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Lasse sat on</span> the back seat of a van and worked on his laptop.
They were driving on some highway through Arizona, right through the desert.
Far away from the road, there were mountains to be seen.
Driver and co-driver were engrossed in a conversation.

Lasse launched Pond.
He looked spellbound at the screen.
First the usual message:

<div class="terminal">
>>> There are no messages waiting to be transmitted.
</div>
He typed "log."

<div class="terminal">
- Jan 14 10:32:08 | Next network transaction in 5m23.029s seconds
</div>
"Wait five minutes. Okay!" he thought.
Pond did not fetch and send messages immediately, but at random times.
This made it more difficult for intelligence agencies to determine whether someone was using Pond at the time.

Lasse: "Nah, not okay."
He typed "transact now."
After a few moments:

<div class="terminal">
(Jan 14 10:40) Message received from sig
</div>
"Shit. Shit!
It's there: the message from Sig," he thought, "Now it's going to be exciting.
Is he free?"
He typed "show."

<div class="terminal">
Hi, Lasse,

I'm with Tim!!! Yes! I'm out! Everything worked out. It's such a super turd there.
I really can't believe it. I'm fucking furious. If I ever meet Fortunato in the dark,
I'm gonna put a cyanide capsule up his ass. But one with a water-soluble shell. And in
the last 30 minutes of his life I tell him Monty Python sketches, one by one.
What a super asshole.

But I'm out for now. That's good. And without a tail. I'm sitting here in a 60-year
old nuclear fallout shelter under Tim's house. His grandmother built it. They were really scared of
a nuclear war. You believe that? Just as everyone was afraid of terrorists later. But now helps
...and us. No signals coming in or going out of here. All sealed.

Joe built me a little clock for my prussic acid capsule so that she always thinks
To be connected to the outside world and not have to release the acid at some point... my dear little one. Was even
Joe modified the capsule's system program. And it works
actually, because: I'm still alive. It's been over five hours. Only if I go out from
here I have to wear a super fun full-body aluminum suit under my clothes: So that the dear
I don't think the little one is going to send home.

Slowly I also wake up and my head no longer turns around in waves. It's crazy what
they give you everything there so you can't think. Better handcuffs than those pills. I think,
I need a real rehab, or better Lilly's chili-ginger-garlic diet: for two weeks!
Remember? In the morning after waking up: two freshly picked and peeled garlic cloves slowly
...chewing, maintaining posture...

I came here with that hitchhiking trick you're probably using right now. I've only had three random
Driver, then Joe, Louis and finally Tim. Six stations. They can't do that, not if Joe, Louis and Tim
drive. And since Joe, I've had the costume on and the clock.

Now I'm gonna chill. Mate is standing here in front of me. When are you coming?

Sig
</div>
Lasse wrote: "reply"

<div class="terminal">
Hi Sig!

Wow, great! I'm hitchhiking with the third station right now. I don't think the driver is from the LBI.
Is a courier driver and obviously has a colleague with him. He's taking me to Phoenix. Trevor's waiting there.
I'll see you in the morning, I guess, I hope...

Great, great, great.
</div>
He saved the message and entered "send".
Pond responded:

<div class="terminal">
>>> There is one message waiting to be transmitted.
</div>
Lasse exhaled.
Two hours to Phoenix.
"Okay," he thought, "time to defuse the first bomb and send their Bitcoins back."

He opened the terminal program and connected to the computer on the Tor network where the first bomb was placed.
He looked at her: 25 minutes to detonation.
Then he typed:

<div class="terminal">
bbomb --disarm
</div>
The program responded:

<div class="terminal">
Passphrase:
</div>
He breathed deeply.

<div class="terminal">
KvTsS#Fwf+w/~pOIet19

Bitcoin bomb disarmed.
</div>
"It's that simple," he thought. "Okay, now transfer back."

<div class="terminal">
bbomb --transfer-all 3J98t1WpEZ73CNmQviecrnyiWrnqRhWNLy
</div>
The moment he tried to press the enter key with his finger, the car drove over a bump.
Together with the laptop, Lasse made a leap to the top.

"Sorryyyy!" cried the driver from the front.
"I haven't seen them."

The command in the terminal program had disappeared.
Lasse presses the up arrow button to get it back.
He wasn't there anymore.
Lasse looked around the car with irritation.
He tapped the arrow key several more times: nothing.
He didn't understand.
Orders couldn't just disappear.
He looked around, his eyes went outside, then forward.
They were heading for a bend with a hill behind it.

"OOOHH SHIT!" he shouted out loud. Behind the hill, directly in front of them, a flying combat robot hovered and slowly turned towards them.
He immediately recognized the model, Reaper MQ-11, brand new at that time, he could stand in the air.
Lasse knew him very well, he was one of his favourite robots, not the fastest, but madly agile.
His heart began to race.
Twice a fireball twitched at the combat robot.
Lasse instinctively pulled up his arms to cover his face.
Half a second later, two rockets whizzed past to the left and right of the car, hitting the ground next to the highway 50 meters behind them.
The shock wave almost pushed the car to the other side of the road.
Lasse was holding his laptop, the driver somehow managed to stop the car.
He and the passenger turned back and stared stunned in the direction of the explosions.

"Fuck!" Lasse yelled, "Fuck! Fuck!"
He jumped out of the car, ran to the back and saw some burning cacti.
Two cars were lying on the road tilted to one side.
His heart was racing.
"Why?" he shouted, and: "Shiiit! You know where I am."

He turned towards the battle robot.
He ran out of rockets and took off.
Then Lasse looked to the left to the sky above the horizon.
"OH!" he thought.
He opened his eyes.
A giant white balloon hovered over the steppe next to the highway, the same one as in San Diego over the sea.
Only this time much closer.
It must have been twenty times bigger than normal hot-air balloons.
And on it was written all over the surface:

<div class="terminal">
AK768-X5P

sZEf\~ofUA+
</div>
Lasse stared at him and nodded slowly.
He knew about the balloon.
It came from TRON.
But what was he doing here?
Now?
It was a clue for him.
No one else would see him but him.
He was thinking.
The bump had prevented him from sending the Bitcoins.
The combat robot was intimidated by the LBI, they had probably become nervous.
It was a little greeting: "We're ready for you and everything."

"Shit," he thought, "they risk killing people for this..."
He clung to his stomach.
"But what does TRON want to tell me?
sZEf\~ofUA+ is a password, sure.
But AK768-X5P? What was that?
I know it from somewhere."

He shut his eyes.
"Wait! Ahh..."

He jumped in the car and typed something on his laptop.
He took no notice of the two drivers, who talked excitedly on the phone.

Let to yourself: "The Five Star Logs.
We have the logs of one of the battle robots we shot down."
He was typing on his laptop.
Lasse: "There!
The combat robot transmits to AK768-X5P ... and this is...
Ha!
That's something.
A note from Sigur:"

<div class="terminal">
AK768-X5P - Control Satellite?
</div>

Lasse thought: "Yes, sure.
This is the control satellite of the LBI.
Must be him.

We got him!
At last! And an access to it right away.
Sigur must know that.
Right now.
Then we can turn the game around.
This is gonna be a thing.
Yeah, they showed.
Now we find their battle robots.
This is gonna be fun."

He smiled and typed.

<div class="terminal">
Hi, Sig,

The LBI combat robots are controlled from the AK768-X5P.
One was just at 34°12'01.8 "N 113°03'40.9 "W, a new Reaper MQ-11.
Can you find him?
The password is sZEf\~ofUA+
I'll get to it after I change cars in Phoenix.
You remember where I am.
But I'll get rid of them in Phoenix.

Lasse.
</div>
He waited until Pond had sent the message and closed his laptop.

Meanwhile there were police cars and fire brigade everywhere.
A policeman waved a trowel in front of them.
You should keep moving.
Shortly afterwards they passed a sign with the inscription
"PHOENIX Arizona, 75 Miles".
