## The Manifesto (2)
<div class="_202y">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Marwin took the pencil</span> between his teeth, stood up and started walking around the room again.
Now and then he stopped, teetering with his feet.
After a while he looked at the mirror wall again.
He smiled warmly in her direction.

"What may be going on in them now?" he thought.
"Why do they do such a boring job of observing him when they have no interest in him as a human being at all?
They spend hours and hours looking at something they are not interested in.
This is a waste of time.
Five days a week like this?
No, I couldn't.

And they certainly think themselves on the better, more pleasant side of the story, on the freer side.
But that's an illusion.
Victor Frankl experienced the greatest moments of his own freedom in a German concentration camp - as a prisoner, not as a guard.
Yes, I can understand that now," thought Marwin and stretched his arms in the air.
"Yes... understand each other.
Get inside his head.
This is it."

He took the pen out of his mouth, sat down again, turned the page and wrote.

<div class="thought">
"6 -- Empathy is the art of thinking and feeling so deeply into another that you understand why he does what he does.
So much so that one tells oneself that one would do the same if one were in his place and had his story.
No child comes into the world with an urge to kill another human being.
We'll buy it.
Children know of no superior race, people, nation or enterprise where some people are worth more than others.
Making such distinctions between people is foreign to us as children.
We're learning that.
That's why we can unlearn it again."
</div>

"Okay," he thought, "what have we got?
1. encryption: the technical basis of our freedom
2. ideas instead of power: A rethinking of the basic ideas.
3. overcoming the opposition
4. keep the initiative sacred
5. aligning our words with our thoughts
6. to think into the other

Hmm.
If I have all that, what more do I need?
What does this make possible in the first place?
Ahh!
Here comes WikiLeaks."
He turned the page.

<div class="thought">
"7 -- We want transparency, openness, visibility of everything in common, of everything that concerns us, of everything that is not private.
When I use or consume something, I have a basic right to know what happened to it before:
Who produced the electricity I take out of the socket, how was it produced?
Who helped build my computer, who wrote my software, and what did he or she do for it?
What do companies do to develop, build and sell a product?
How does the state ensure my safety?
What does he do for it?
Is he killing other people for it?
How many? Who?
Does he torture for it?
Is he suppressing?
It's my natural right to know, just by using something.
And others have a natural right to know everything about what I do not just for myself.
Open, free software took up a fundamental right before it was formulated.
In fact, it applies generally."
</div>

He smiled and breathed deeply.
"Uhh... That changes things.
And that would probably go wrong without the first six steps:
A truly open, transparent society.
Uh-huh."

He's going for his nose.

"And then we need...

He turned the page and went on writing.

<div class="thought">
"8 -- We need courage, lots of courage, to overcome fear.
We get scared when we lose contact with the world and other people ...
and to ourselves, to what we can do.
Then we begin to imagine all sorts of things, what is or what will happen.
And the more afraid people are, the easier they are to manipulate, to control.
Intimidation makes slaves.
The answer to fear is courage: seize life, approach strangers and look at yourself - in peace.
Courage is the power to overcome all fear.
And courage is contagious.

And then anger.
She is the warm, powerful force in our bodies.
Anger is not aggression.
When anger meets fear, it becomes aggression, otherwise not.
We need a lot of anger to overcome paralyzing hope.
Hope, which is made from the outside, paralyzes you.
We don't need this.
We always have enough hope of our own in us.
We just lose sight of her sometimes.
Hope dies last."
</div>

He turned the page.

<div class="thought">
"9 -- people can't be numbers anymore.
A person cannot be recorded in a statistic.
It will kill him.
The sentence "Eleven people died in a plane crash" kills the people a second time.
Because in reality, they were Anke, Helmut, Tim, Trevor, Silke, Antje, Lisa, Jochen, Leon, Aischa and Finn.
They all died in different ways, all at different points in their lives.
They all have other connections to other people.
They were just in the same place together when they died.
People can't be numbers anymore, people are people."
</div>

He put the pen on the table and looked at the text.

"Okay, people are people.
That's a fine last word.
I think that's everything," he thought.
"Nothing more needs to be done."

He took the pencil and wrote:

"Cipher,

real ideals,

overcome the opposition,

free development,

Thoughts instead of words,

...feel inside,

Openness,

Courage and anger,

man as man."

He put the pen back on the table and stretched.
He breathed deeply.

Marwin: "And who's reading this now?"

He shut his eyes.
After a while, he opened it again and said, "Nobody."

"Hmmm.
We always have enough of our own hope in us," he says aloud.
"Stand here!
Okay.
"Then I..."

He raised his hands, looked up at the ceiling with big eyes, "I need magic!"
Then he looked intensively at the booklet, rolled his eyes, spread his fingers over it in strange shapes and said mysteriously as he moved his hands up and down:
"Abura kadabarura, find your way to... Marlene... Oskar... Julian... Toby".

He looked at his fingers in amazement and said to them, "Hmm?
What do you want?
What do you want now?"
He nodded.
"Yes, I can imagine.
I'd die for a keyboard.
And if it was just a trouser keyboard on a Raspberry Pi or an Olimex board.
I miss it so much!
Typing.
buttons.
Shit!
Now that would be even better than a mate at sunset on a Berlin roof."
He took the pencil and wrote:

<div class="thought">
"10 -- love of technology"

He looked at the list and nodded.
"It's nice now.
Exactly, we must develop a love of technology on a broad scale.
Technology is the frozen intelligence of all people, frozen knowledge, frozen mind, at my service at all times.
But only if I work with others.
Only if I build on what others have done, if I continue to build on mine.
We learn to work together, through technology.
Helping others.
Every programmer is helpful."
</div>

He shaped his fingers again in the most absurd way and moved them over the fully written sheet: "Abrakadubradabra," he said aloud, "fly to Marlene, fly to Oskar, fly to Julian and Tobi.
