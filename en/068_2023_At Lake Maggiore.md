## At Lake Maggiore
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">>Marlene sat with</span> a big, round straw hat and sunglasses on the terrace of a holiday apartment on Lake Maggiore, in the north of Italy.
She looked across the lake, at the mountains on the opposite side.
The terrace was overgrown with agaves from left and right.
Also on the terrace itself there were pots with large plants everywhere.
She sat cross-legged in the shade on a basket chair, her hands in a meditation gesture, her eyes closed.
After a while she opened her eyes, put earplugs into her ears, laid her legs on a small table, took the laptop on her lap and started typing.

Marlene at half volume: "Hi!"

She listened.

Marlene: "Everything is very quiet here.
I can work.
From the other side of the shore the terrace is theoretically visible, but that's four and a half kilometres.
It's okay."

She listened.

Marlene: "Yes, of course I have heard about the new Leaks at Google.
I know the two guys who do it.
It's really unbelievable: The Android people have actually managed to reinstall the back doors for the third time.
For the third time!
And swore twice before never to do it again.
What are they doing?
They'll lose all credibility.
I know some normal people who are going through the roof right now."

She listened.

"No, that's bullshit, they didn't find them.
They are still there and active."

She listened.

"Of course I'm sure.
I just got a Ricochet message.
They found others, maybe they need some scapegoats.
Maybe it didn't go entirely clean, maybe they hacked into other accounts internally and sent the leaks out through them.
But definitely, they're still there, not blown up, and hell bent on reporting new vulnerabilities as long as they're implemented."

She listened.

"Yes, exactly.
I agree.
They're heroes.
These are the heroes of modern times.
We need them.
These are the revolutionaries of whom pictures will later be hung on the wall.
At first you want to hang them yourself, later you want to hang their pictures.
It's always been: Jesus, Gandhi, Martin Luther King, Mandela."

She listened.

"Yeah, yeah, sure.
I can't think of anything I'd rather do.
I don't want to end up as a picture on some wall either.
But it will be.
People love to admire others.
It's more comfortable than being a hero yourself.
We could all be heroes..."

Marlene heard a blow against the metal gate at the entrance and turned around in horror.
She looked.

Marlene: "Hold on."

She saw Luigi come in with a big bag.

Marlene: "I'll call you back.
See you in a minute."
She turned around.
"Luigi!
I didn't know you were coming now."

He smiled at her, put the bag on the table and kissed Marlene's cheeks left and right.
She smiled and reached for the bag.

Marlene: "Ahhh, Italian cookies!
Mmmhh.
Benissima."

Luigi was in his mid-thirties and had lived in Luino, in the Italian part of Lake Maggiore, near the border with Switzerland, for 20 years.
He had set up a local hacker space, not the biggest one, there were only 5 or 6 members, but they had the odd good hack behind them.
For example, they had built the coffee machine, which you first had to convince via encrypted jabber chat that you need coffee now.
The coffee machine analyzed how active one was in open source projects on Github.
If it wasn't enough for her, you had to find one of the tricks to get her to cook.
You presented this two years ago at the Chaos Communication Congress in Leipzig.

The request to accommodate Marlene had come from an old friend, and he had immediately agreed: How could he do anything but help the woman who had laid the foundation to repair the Internet from scratch, the woman who had shown them the potholes in the data highways?

Luigi: "Did you see?
Your class sent you a video on WikiLeaks."

Marlene: "No.
Really?
My old class?
Now?
What are they saying?
That I should come back and face a fair trial?"

Luigi: "No, not at all.
I think it's super cool what they say.
They want to celebrate you and are on strike until your public image is corrected.
They celebrate that you hacked the vulnerability database and are on strike against propaganda in Germany."

"What?!?", Marlene took her legs off the table and started typing something into her laptop.
Then looked spellbound at the screen.
Luigi stood beside her and put his hand on her shoulder.
He smiled.

Luigi: "It's good to hear that, isn't it?"

Marlene looked at Luigi briefly out of the corner of her eye.
After a while she put the laptop away, got up and went to the terrace railing.

"Not there!" cried Luigi, "not to the railing.
It's visible."

Marlene stopped.
Tears were rolling down her cheeks.
She grabbed the railing with both hands and closed her eyes.
Luigi came and put his arms around her from behind.
They stood like this for a few minutes.
Then he pulled her gently back.

Luigi: "Come! Out of sight."

Marlene broke away from him and sat back in her basket chair, took her laptop and watched the video again.

Marlene: "Do you have a piece of paper and a pen?"

Luigi: "What?"

Marlene: "Paper, pen, just like at school."

Luigi: "Yes, I think".
He went away and came back with a pad and a pen.
Marlene put the pad on her laptop and started writing:

<div class="handwritten">
Dear Sophie, Anni, Kevin, Aischa, Lukas, Janis, Lisa, Leonie
Felix, Berem, Jannik, Jannick, Philip, Moritz, Antonia, Aylin,
Johanna, Tom, Jonas, Devin, Marie, Lena, Nils, Marwin, Emilie,
Viviane, Tatiana,
<br /><br />
I'm sitting here with tears in my eyes because I just saw your
I saw the video. It's so good to hear from you. I knew
really not whether I had lost you or not. And you
seems to be the only ones from my Berlin days
really know each other - except for some hacker friends, who remained
are. There too, some have turned away. We have powerful
Enemies.
<br /><br />
Your video will be noticed by the public. I think you know you're gonna run into a lot of headwinds because of this.
A full war is raging on the Internet, with dead, wounded, prisoners and tortured.
You've all been under separate surveillance for two years.
All your phones, all accounts, Facebook, WhatsApp, email, even Threema, Telegram, even Signal and Xabber.
As soon as weak points are built in again, they do it.
It's best if you all switch to secure laptops.
I will ask someone to come to you and help you set up secure communications.
<br /><br />
Be prepared for a much, much headwind.
What you are doing is great, but it is a real danger for some powerful people who want to get rid of it by all means.
You are dangerous because you attract attention, arouse interest.
This is their biggest problem.
They don't want us to notice what's happening in the world right now.
But you can also be sure that from now on, a lot of eyes from the internet will be looking at you to protect you.
They will do things that will give you a better chance of getting out of it safely.
Your opponents, our opponents, are not afraid of murder either.
That's why: Go out in public, make noise so people can see you.
You need TV, press, Twitter
... They have respect for the public, they have a hell of a fear of the anger of ordinary people.
This is their real danger.
They need the normal people, they need you, because somebody has to do the work, pay taxes and buy things.
<br /><br />
I think you have a big advantage: you will be surprised that a whole class closed for more publicity, for the truth, is on strike.
That's strong.
They will need a few days or even weeks to learn how to deal with it.
This is your chance.
Until then, the world must know you.
Then you can come through.
They will want to separate you, try to make enemies of each other through parents, siblings, friends, teachers.
Do not be frightened and do not let promises give you hope.
And use the best weapons available: GPG, Tor, OTR, Tails, Cubes, Pond, Ricochet and VeraCrypt.
And Linux or BSD, of course, just Linux or BSD!
<br /><br />
Good luck to you.
I hope to see you again sometime soon...
<br /><br />
Marlene
</div>
Luigi: "Hey!
You're right.
You are in great danger now.
But we can fix that.
I will take your letter with me, scan it and send it to WikiLeaks.
Your famous mailbox is working again."

"Can you do it from here?" Marlene asked.
"I don't want the paper to leave the apartment."

Luigi: "Okay."
He went away and came back with an older digital camera.
He showed it to Marlene.
She nodded.
Then he took a picture of the letter.
Marlene took it, went to the patio fireplace and lit it.
Then she went to her laptop, inserted the camera's SD card and started typing.
