## Taskforce Marlene
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Amelie sat behind her studio table</span> and stretched a Marlene mask in front of her face.

Amelie a little muffled behind the mask: "Boys, girls and others, are you ready?"

Joe and Sven had their fingers crossed.

Freddy: "Always... b.reit!"

Amelie: "Go!"

The lights came on, the camera turned to her.

Amelie: "Hello and welcome to Amelie Out of Berlin.
Everything out of Berlin.
Today... today I have a real surprise for you.
This is gonna be fun.
All live!
That's why this is a half hour earlier than usual.
The real Out of Berlin starts at 1pm, after breakfast, as always.
But we have to prepare something."

She took off the mask and held it up to the camera.

Amelie: "Of course it is about Marlene.
During our great campaign last Tuesday, more than 20,000 people all over Germany hung up the Marlene pictures.
20 000!
You guys are awesome!
12,000 more than the week before.
And that's why we're going to detonate stage two.
BANG!
You want to know what stage two is?
And then... comes... with..."

She got up, went to the door, put on a jacket and stormed out.
Joe, Freddy and Sven after them.

Joe: "Wait, I'll take you from the front."

He passed her down the stairs.

Amelie into the camera: "Yes, you are curious, aren't you?
This is about Marlene!
Oh, sure, you already know that."

She giggled.

In front of the front door she looked around and a big black Hummer limousine turned the corner.
She stopped and everyone got in.

Oskar was at the wheel, wearing a Marlene cap and turning backwards.

Oskar: "Where is the party?"

Amelie: "On the feces!"

Oskar: "Kottbusser Tor. Okay!"

Amelie into the camera: "Because we have registered a demo today and therefore everything is already blocked there."

Oskar from the front: "But if you now think that the demo is the surprise ... No, no."

Amelie imitated Oskar: "Nah, nah."

Amelie: "This way we wouldn't get so many TV stations together."

The car stopped at a police barrier in front of the traffic circle around the underground station at Kottbusser Tor.
Oskar turned the window down and spoke to a policeman.

Amelie: "Back there I see N-TV, ZDF, time online is also there.
Well, that looks pretty good.
You want to know how we got them all to come?
I'm sure...
But I'm not telling you that yet."

She giggled again.

The police waved the car through.

In the cordoned-off area of the roundabout there were another three limousines.
Oskar steered his behind it.

"All there!" Amelie shouted into the camera.
"Big question: Who... is... in there?
Please make suggestions in the comments below the video."

She opened the door and got out.
She went over to the rather unkempt lawn in the middle of the roundabout.
There were twelve colorful chairs arranged in a semicircle.
She looked for the middle and looked around.
Six television teams were kept at a distance of about 10 metres by stewards.

At the same moment the doors of the other limousines opened.
At first you could see an umbrella coming out of each door with a picture of Marlene on it, but you couldn't see who was hiding behind it.
Men and women, very different clothes. 11 umbrellas moved towards the circle of chairs.

"Krooooonck!" shouted an onlooker.

You could see a hand waving from one of the umbrellas.

Everyone found their chair.
Obviously the order was important.

Amelie in a dark voice: "So!
The hour... of reckoning... is here!"

She giggled.

Amelie normal: "Not accounting, no!
Life insurance!
... not quite.
Just love and thanks!
I'll start:
Liiieeeeee Marlene..."

Amelie pointed to the first umbrella.

Julien, a well-known YouTuber took the umbrella down.
Two spectators screamed.

Julien: "Dear Marlene: My YouTube account was shut down for two weeks a year ago.
At first I hated you for it, just like the hacker who hijacked my account... until I realized... ...which means we have all these holes on the Internet.
Where anyone who has the keys can get in.
All you did was hand out the keys.
Edward snowden didn't do this.
That's why not soooo much has changed through his documents.
I am eternally grateful to you for doing this.
And that's what I'm here to tell you today.
And I invite you to join me on my blog.
If we have an appointment, then I will also invite the German Chancellor, she likes us bloggers.
And when she comes, maybe you can ask her what she thinks about the holes."

Julien looked to the left to the second umbrella.
It went downstairs.

Another famous YouTuber: Kronck.
Again screaming from the spectators.

Kronck bowed.

Kronck: "Hmmm.
Joah.
Marlene.
Cool!
From me too: Girl, keep it up!
And survive.
And I don't want to bore you all with a story very similar to Julien's.
Except they flooded my channel with commercials, the weak-minded."

He looked to his left.

The next known blogger took down her umbrella.

Bibbi: "Yo, so!
We could go on like this and tell one story after another.
But then we'll just talk about us.
That's better:
MARLENE!
From now on I will show a small picture of you in the lower right corner of my blog.
In every episode.
Until you can walk around free here in Berlin."

She looked to her left.
The next umbrella went down.

Timo: "From now on, I'm ending each of my shows with:
And then I'm also of the opinion that the secret services should open up.
Someone in ancient Carthage once did that successfully."

He looked to his left.

Dagi: "Marlene, I rent the huge billboard at Potsdamer Platz, show your picture and write on it what you send me.

She looked to her left.

Luka: "I rent perimeter ad in Fifa 24 with the same picture and the same slogan."

Simon: "I'm doing a show with what politicians answer the question: "What do you think of Marlene Farras? Courageously not?" answer.
I look forward to it like a child..."

Paddy: "I'm making a Minecraft game where you have to try to hack into the NSA vulnerability database.
Then we'll play it together on my channel."

LaFluid: "I've been covering you for a while anyway.
I'm going to open up a new side channel and cover everything that the girls and boys around me are doing here."

Ibali: "Yes, cool!
"And I'm going to perform on the canal..."

The last two umbrellas remained up.

Amelie: "Joo. You thought this was going to continue.
Nope. We only had 10 places for bloggers, first come first serve.
Now comes Timo.
Timo is not a blogger.
You don't know him?
No.
But his work might.
We don't know for sure.
He's been working undercover until now.
But you know them... I'd say probably..."

Under the 11th umbrella you could see a rocking hand movement.

Amelie: "But... what you know is the TOY CREW!
The most courageous sprayers of Berlin.
Live here today... for the first time."

The umbrella went down.
Timo: "Hi guys.
I'm a little excited.
Of course, I haven't done any of the things Amelie's hinted at now.
There were always others.
But I intend to do something like this:
I'll get a crew together and then we will spray one or if possible several boxes.
Boxes, that is, subway boxes. Car.
Jo, complete and in color.
With Marlene and a few lines next to it.
Let's see."

Amelie played excitedly: "Timo!
But that's illegal!
Surely you can't spray a train of the Berlin Transport Company?"

Timo casually pointed with his thumb to the left to the last umbrella.
The umbrella went down and the mayor of Kreuzberg appeared.

Mayor: "Exactly.
Before you use the walls of houses in Kreuzberg again, I have organized three subway cars for you.
And they'll be up and running normally when you're done spraying.
I have made an agreement with the BVG.
They'll keep it on for at least three months."

She turned to the cameras.

Mayor: "And, Marlene, to you!
Cool!
It's cool!
Come here to Kreuzberg as soon as it's safe for you.
We'll find a nice place for you to live.
Kreuzberg is good for brave people."

Amelie: "Yes!
Right!
Yes!
We need that!
Courage!
Come on, guys...
There are even still courageous politicians.
Let's make more.
The U.S. wants Marlene.
But we're not giving them up!"

Amelie went to the chairs and hugged them all, one by one.

Amelie into the camera: "That's it for today!
That was Out Of Berlin.
And we're gonna go celebrate.
You do.
And hang some more pictures..."
