## The speech
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">The picture in the school class</span> had changed.
Everywhere were now black, somewhat bulky laptops on the tables, many mini computer boards, cables, power supplies, USB sticks, screwdrivers, a soldering iron.
Everywhere small groups were sitting around one or more laptops.
They typed, discussed and gestured.
There were Marlene pictures hanging all over the windows.
Kevin stood with Sophie at the front of the blackboard and gave an interview to a journalist in front of a TV camera.

Kevin into a reporter's microphone: "Yes, things have changed for us!
We have so much attention.
Many school classes look to us.
It's good for Marlene.
But I also know what Marlene would say now if she were here:
"What do we do now?
Waiting is not enough."
Attention is one thing.
Strike and party?
Yeah.
But that's waiting.
And that's not enough.
And that is why something else has come to the fore for us.
We want a new kind of school.
We want to change schools.
Look around you!"

He pointed into the room.

"We've all learned more in the last three weeks together than in the three years before.
And I really mean it.
Not only technically.
Human, too.
Working together, trusting each other, helping each other.
This is because it is serious for the first time, because it has to do with real life.
Most people here have never known how easy it is to work with hundreds of thousands of people on the Internet anywhere in the world.
To work together properly.
We did not know that there was a whole culture of knowledge exchange and mutual help.
Open source, Git...
It is so much fun to help others.
And to get help.
And to see what it is like when what you do yourself is used by others.
We haven't had any of this in our school in the last few years.
No Git!
Imagine that.
The whole school without Git.
None of it.
And Git is at least as important as Linux.

<div class="infobox" id="git"></div>

Compare this with a normal situation in any classroom:
Most of the time we are not fully involved in class, which is a lot of waiting, boredom and endurance.
Here we are always fully involved, here we live.
Everyone's wide awake.
And we have super people coming to us, really well-known people, especially from the hacker environment, but more and more others as well.
They tell us what they know about computers, programming, computer administration, our legal system, our political system or whatever.
That which they are enthusiastic about and know more about than others.
We had lawyers, policemen and businessmen here.

It's a super trusting atmosphere.
If someone doesn't understand something, he asks.
If someone doesn't care about something, he goes somewhere else.
Instead of the many imaginary tests of teachers, we now have real life as a test.
This is real.
They even make exams fun.
We have attacks on our computers here.
We have to communicate undercover.
Can we make our computers so secure that they are not disturbed?

We learned a lot, for example how to turn such a mini-computer here into a high-security web server in twenty minutes that nobody outside knows where in the world it is.
We learned how to make a laptop pretty tap-proof.

When this action here is over, I definitely want to continue with school in the same way, and the others here certainly want that too.
Personally, I don't give a damn about the Abitur in the meantime.
It's all fake, it's all made up.
There's no real value in it.
What do you mean, graduate from high school?
That doesn't mean anything.
And nobody really cares - it's just a pretense."

Reporter: "Aylin, Jonas and Devin have left the group.
What do you say to that?
Are there any other students considering dropping out?"

Kevin: "Aylin's mother had a nervous breakdown.
She is a single parent and has just lost her job, so Aylin did not want to stay here.
She'll be back as soon as she can.
Jonas and Devin, the pressure was too much.
The parents didn't really go along with that either.

Our opponents are called NSA, BND, also other intelligence agencies.
For them, this is an unpredictable chaos.
Marlene is really, really dangerous for her.
And we're in it now.
Because if Marlene becomes known, then more people look at what's actually going on, and no secret service wants that.
There are now over 200 classes in Germany who are celebrating and striking with us.
And there are more every day.
Now imagine what will be going on in the secret services if we manage to straighten out the public image of Marlene ...
When most people understand that it was all an act with her."

Reporter: "In the meantime, they have other goals. They demand the abolition of compulsory education and free choice of teachers."

Kevin: "These are not "demands".
It's common sense.
Do we need compulsory education?
No one has to force me to learn.
I like to learn.
Just don't take pointless crap from people who haven't seen life.
Look around you!
Does any student here need compulsory education?
Should any student here spend even a single hour with a teacher who does not interest him?
That's hokey!
We are all old enough here to choose our own teachers, and the teachers are also old enough to choose their students.
We are learning at a speed that is breathtaking.
This is where real talent suddenly shows up.
It's a great learning environment.
When I imagine what we would have done and learned if we had had such a school all these years...

This cannot exist in a hierarchical school form, where it is prescribed what we are to learn, when we are to learn it, from whom we are to learn it, even how.
By people who have been teachers all their lives and know nothing about life.
That's ancient rubbish.
Nobody here wants that anymore.
I don't think you'll find a class in Germany with five students who would prefer this to this."

Reporter: "You say you want to "straighten out" the picture of Marlene.
But it is also possible to see it as something illegal to break into military computers and steal documents from them.
After all, we need agreements in a democracy on what is permitted and what is not.
You can't just walk away from that..."

Sophie turned towards them.

Sophie: "The documents that Marlene gave to the public belong to everyone!
They affect us all.
That's why they belong to all of us.
We live in a democracy and without these documents I cannot form a democratic opinion.
I can't just say of anything that belongs to everyone that it's a secret now.
And then also keep the reason for this secret.
If someone kidnaps your child, do you have a right to break into the house where he or she is and get him or her out?
Sure you did!
Democracy is more important than secrecy for tactical reasons."

Reporter: "Hold on!
You have no right to break the law to get your child back."

Sophie: "If it won't hurt anyone?"

Reporter: "I think you have to tell the police, and then they can break into the house."

Sophie: "Then this is a mistake in our laws that needs to be corrected.
A bug.
Think about it: there is the house, you are not hurting anyone, you might break a window and get your child out.
That must be allowed.
And if laws allow the secrecy of documents that have public significance, then that is also a mistake, like a bug in a computer program.
We must correct such laws and publish new versions of the law.

And in the secret services there are obviously so many undemocratic and anti-democratic activities that it would be the worst thing for them if these activities came to light.
And that's why they're trying everything they can to stop us.
They rely on secrecy.
Imagine if we could prove that the secret services ensured that Aylin's mother was bullied and released.
And that they put even more pressure on her afterwards, frightened her.
That's why she collapsed."

Reporter: "Other countries see it differently.
In England, Marlene is seen by the media as a terrorist."

Sophie and Kevin looked at each other.
Kevin shrugged: "This is England.
I don't know the situation there."

Reporter: "What are your next steps?"

Sophie: "We are networking more and more with the other classes, now throughout Germany.
There are 217 classes on strike.
And even more consider whether they will take this step as well.
We're getting a tremendous amount of encouragement.
Many students are using our new, fairly bug-proof communication system."

She smiled.

"I think even WhatsApp and Facebook are starting to notice that.
There are about 400,000 school classes in Germany.
This is what we are currently designing our server capacities for.
In our team there are some boys and girls from the top league in the hacker environment.
Marlene is also actively involved.
I don't know how she does it, but she's in it.
You get us everything we need.
We get the best equipment available."

Reporter: "If you now have secret services against you, doesn't it make you feel queasy?
Aren't you afraid?"

Kevin: "Of course we are afraid.
Lots.
But we also have courage.
Marlene has courage.
She's still active, she's not using all her strength to hide.
She actually helps more than we help her.
Well, we can put a shovel to our courage.
And then we have you as life insurance.
If you report about us, if you make us known, then secret services are more careful with us.
"Our lives depend on what you send and write.

Reporter: "Your life?"

Kevin: "Sure.
Our lives."

From the other corner of the room someone shouted: "The American president is on television!
This is the address to the Secret Service."

Everyone turned to the large screen hanging at the back of the classroom.

"Louder. Louder," cried one of the students.

Kevin took one look at Sophie and the reporter and said, "Now I'm curious to see what this is going to be."

"Quiet," cried another student.
Everybody looked spellbound at the screen.

President: "Dear compatriots, this is my weekly address on our situation, here in the country and worldwide.
Today I want to talk about one current topic in particular, and that will be a little different than usual.
This is because even in the best family you can disagree at times, even argue.
And I don't mean Republicans and Democrats, because traditionally we have a lot of arguments between us."
She smiled.
"No, I mean facilities that form the backbone of our great nation:
our global corporations, our intelligence services and the political center of the U.S.: our governmental organization.
Our companies lead the technical development worldwide.
Our secret services are the best and most powerful that exist and have ever been.
They risk their lives so that we can live here in peace and quiet in our own way.
You have done a great job over the last few years.
But terrorism has challenged us more and more in recent decades.
Much of this struggle is now taking place on the Internet.
To ensure that the Internet remains a safe place for all of us, our secret services have set up a surveillance system over the last few decades that notifies us at a very early stage when terrorist attacks are being prepared, riots are forming or attacks on America are being prepared.

But in some places we have overstepped the mark in these efforts.
Against the will of the companies, the secret services have built surveillance software into programs such as Facebook Messenger, WhatsApp, YouTube, Instagram and so on, making the communications of billions of people more insecure.
Many people have lost confidence in our companies and turned away from them.
We believe that these measures went too far in some cases.
We are currently starting some programs to analyze and correct this.
One of them I would like to present today: a seal of quality like the "Energy Star", which is intended to restore confidence in our world-class products. The "Security Star."

Everybody in the room laughed spontaneously, running up and laughing at each other.

Someone shouted, "NSA free! Now 100% guaranteed."
And laughed out loud.

President: "Any product bearing this seal is free of back doors and intentionally installed vulnerabilities."

More laughter in the room.

President: "Most of the American companies I have spoken to about this are willing to do so.
They will subject their programs to regular, rigorous government scrutiny.
The secret services, I can say so openly here, were not enthusiastic about the plan, but they accepted it in the end.
There is also no longer any possibility for them to legally intercept and decrypt communications from such programs".

Luke: "So they make it illegal..."

Kevin: "Luke, there is something new in what she says.
This means that secret courts like FISA can no longer give permission.
The companies won't let her back in.
Google is pissed off at the secret services. If they don't have to do it anymore, they don't do it anymore.

President: "I know a lot of trust has been lost.
I'm willing to fight to get that trust back.
That is why I will bring a bill to Congress in the next few days, which will exempt from punishment all reports of technical security holes, weaknesses, backdoors.
Whistleblowers who report such security breaches are guaranteed by this law that they will not be prosecuted.
even if they are company secrets."

It became quieter in the classroom.

President: "And today I have signed an amnesty order for all whistleblowers of technical security breaches who are currently being detained or charged in our country.
According to our count, that is currently about 240.
You will be discharged as soon as possible and you will be entitled to compensation."

It was quiet in the room.

"Wow," Oskar said into the silence.

Sophie: "What is this?
I don't trust that roast."

Kevin: "They're laying everybody off... Hi!
That's great.
You must do it now.
The President will see to it.
It is the weekly national address.
That's awesome!
This is really cool!
Even if that doesn't mean they've gone from wolves to sheep now."

Sophie: "But then how can we trust them?"

Oskar: "We can't.
But the situation for whistleblowers has improved.
A few people I know will now dare to report weaknesses that have not dared to do so before."
He pointed at the television set.
"This, this is a disaster for the intelligence community."

Sophie: "What about Marlene?"

Oskar: "No idea.
It'll be good for her too, I think.
But she's not exactly a whistleblower."

Kevin: "That sounds a bit like internal war."

Oskar nodded.

Oskar: "Yes, they argue internally everywhere and about everything."

The reporter approached Sophie and Anni.

Reporter: "Sophie, Annie.
May we interview you about it for the evening news?
I think this is going to be a news story."

Sophie: "Yeah, sure."
