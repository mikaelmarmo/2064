## WikiLeaks TV
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">A group of journalists</span> sat around a table in one of the meeting rooms of the CNN editorial office in Washington.
Together they looked at a huge computer monitor.

A journalist rushed in: "Quick! Make it quick:
On WikiLeaks TV, here we go!"

A second journalist pressed the button on a remote control: "Shit, do you know what it is yet?
I keep getting calls."

First journalist: "That must be a super hot thing!
This will keep us energized for the next few days, maybe weeks.
I get messages in, the hairs on the back of your neck stand up.
The darknet is in full excitement.
I tell you."

Third journalist: "All major government buildings on the east coast have been evacuated.
But we can't get any official word on what it is."
He pointed at the monitor.
"What is it now?"

WikiLeaks-TV showed live pictures of nine different places in tiles, each with local time inserted.
On one was the Golden Gate Bridge, on the next the MGM Hotel in Las Vegas, on one the White House, Freedom Tower in New York, Sears Tower in Chicago, the Pentagon.
Below right is the picture of the NSA headquarters.
The time there is 6:07.

First journalist: "Hit targets.
These are potential targets."

Third journalist: "Ajjj. Shiiiit... A live hit.
Anonymous once announced that."

First journalist: "And the station failure today is also said to have something to do with it.
They must have hacked a satellite before."

Third journalist softly: "Wow."

First journalist: "You see those two F22s in the background, there behind the White House, and here one behind the Pentagon, to the right of Freedom Tower.
They're everywhere.
They're circling.
I think they've got every plane out there that they have."

Third journalist: "Do we have people on the ground?"

First journalist: "Four teams are on the move with cameras, a bunch more without."

Suddenly the picture of the NSA headquarters enlarged and after two or three seconds took up the whole screen.

"What now?" cried the third journalist.
Everyone moved closer to the screen.

First journalist: "Take this to the main program.
Right now, main program.
Here we go.
We want to hear that live.
Subtitles here to me.
Quick, write NSA headquarters.
Terrorist attack may be imminent.
Central buildings have been cleared nationwide.
Did you?"

The camera panned away from the NSA building to some black spots on the horizon.

Second journalist: "What is this?
Missiles?
Missiles!
Oh, oh, oh.
Where's Tom?
Tom!
Tom, what is this?"

Tom stood up to him: "Hard to say.
Maybe I... yes... maybe Reapers.
Definitely armed combat robots...
Yeah, those are Reaper MQ-11s.
Shit!
These are new.
They can make hell."

Third journalist: "How many are there?"
He counted.
"Four, five, six, seven."

Second journalist: "15.
it's 15.
What are they up to?"

First journalist: "What are they up to?
They're attacking NSA headquarters."

Second journalist: "That's not possible.
They can't fly there.
This is a no-fly zone for combat robots.
All combat robots are programmed to avoid entering no-fly zones."

First journalist: "Maybe the combat robots don't know that they can't fly there.
You just do it anyway...
Shit, they're fast."
Small flashes of lightning twitched at some combat robots.
"What is this?
Oh, no!
Shit.
They're firing at the main building."

Second journalist: "Woah, what are those explosions?
This is crazy.
The first one blew up the whole top corner.
Oh, my God.
They'll take the whole building down with a few shots."

Third journalist: "They are circling.
Next hit.
The building won't hold much longer.
I hope there's nobody else in there."

Second journalist: "But who is this?
Who does that?"

Third journalist: "Full of madness!
The whole right side is collapsing!
Fuck!"

First journalist: "Who goes on air?
I have to stay here, more information will be coming soon."

Second journalist: "On my way.
Oh, shit!
What does it say at the bottom of the screen?


<div class="terminal">
More information in ten minutes.
</div>
This will be a letter of confession.
It's orchestrated.
WikiLeaks is on board with this.
Shit, what am I saying?
What is the message?
America under attack?"

First journalist: "We've been through this.
Take 'Terrorists Attacking Our Nerve Center'.

Second journalist: "And WikiLeaks is a terrorist organization!"

First journalist: "No, 'WikiLeaks is the mouthpiece of the terrorists'.
It's obvious now.
Take this as your first message until we know more.
Hopefully others will follow suit.
or it'll be a complete media circus."
