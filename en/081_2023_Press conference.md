## Press conference
<div class="_202z">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">The classroom was >/span> filled with rows of chairs.
In front of the desk were three tables with chairs behind them and a name card each.
Kevin and Sophie sat next to each other at the middle table.
Two camera teams positioned themselves in the room.
Luke was working on a computer with another student.
Anni checked press passes with a student at the entrance.
The room slowly filled up.

A journalist shouted from the middle of the room: "Kevin!
What's new today?"

Kevin: "Just a moment, Christoph.
There's really something new.
Breaking news.
Don't you want a better seat over here?
It's worth it.
And get on the Twitter..."
He smiled at him and then turned to Luke, "Are you ready?"

Luke: "In a moment.
Almost everything is going."

A journalist shouted from the back row: "How many classes are on strike now?"

Kevin: "Same, same.
It's all here on the slip of paper."

Anni called from the door: "1220."

The journalist shouted, "Wow!" and typed something into her iPad.

A little bit to her right was Amelie, in front of her Joe with camera and Sven with microphone.

Amelie quietly, a little conspiratorial: "Welcome to the special edition of Amelies Out of Berlin!
We snuck in here today because we learned from well-informed hacker circles that something is happening here today.
A world premiere.
A game changer, that's what it is.
Can't stay away, can we?
Ah, here comes our tipster..."

Joe turned to the door with his camera.

Oskar entered the room and looked around.
He waved at Amelie.
Then he went to Kevin, put a USB stick on the table and said, "The transcript.
The stick is clean.
You can put it right in your pocket."
Kevin got up and brought the stick to Lukas.
He nodded.

Lukas: "Okay, then we are done.
I'll upload this and we'll be good to go."

Kevin stood in front of his table.
He looked into the room.
It was packed full by now.
Some journalists had to stand.

Kevin: "So, I think we can begin..." He cleared his throat.
"Welcome!
The procedure for today is that we first have a whole new thing for you, then we say something about the strike network and then you can ask questions.
Does anyone not have internet?
- Good. Okay." He looked at a small monitor next to him.
"Wow, we have over 600,000 Internet viewers.
All right.
Point one.
Luke!"

The beamer went on and the face of the director of the NSA appeared.

Kevin: "You know this gentleman, don't you?" A soft laugh went across the room.

Kevin: "Okay.
Here's what he said in Congress a week ago.
The so-called Cyber-Terrorism Report."

Luke pressed a button.

Director's voice: "We must step up our efforts.
In the wake of last year's major attack, we have seen a radical increase in attacks on our infrastructure.
This blatantly refutes the cyber-terrorists' claim that publishing vulnerabilities leads to greater security on the Internet.
The opposite is the case.
Hundreds of thousands, perhaps millions of criminals have used the publications to invade people's privacy, to cheat, blackmail and destroy their data.
And terrorists now communicate freely wherever they want to communicate.
without any interference from us."

Luke pressed a button.

Kevin: "So much for the NSA chief two weeks ago.
I do not want to comment on the content here.
That's not what we're talking about today.
Today we are concerned about his voice.
The way he speaks, the way he sounds.
Because there is a second recording we want to play for you.
And it was taken five days ago, here in Berlin,
shortly after the President of the United States announced the amnesty for whistleblowers.
The conversation was at headquarters on Chausseestrasse."

Luke pressed a button.

Director's voice: "The next time I meet the little spitfire noodle from the white house, I'm going to get her.
We have a responsibility here for the greatest nation in the world, and for our friends too.
We cannot accept that she castrates us in such a way.

We have, damn it, built the best news system the world has ever known in the last few years.
We together.
We were so close to being able to look into every computer in the world live that we could see the tiniest changes in political movements around the world from the screens at Fort Meade, that we could track terrorists anywhere, any second of their lives.
And we had kept the population relatively calm, even in Europe.
Relatively calm.
Protest: Yeah.
Real consequences: No!
Not even one percent of them was really interested.
We were so close...
And then this shit now.
I don't care what she said in that speech."

Kevin: "The little spitfire noodle, you noticed, is the President of the United States.
His top boss.
And he doesn't give a damn what she says.
In doing so, he is also disregarding US legislation, the legal system, the political system; he is giving America the finger.

Whispers and conversations in the room.

Kevin raised his arms: "There's more.
Does that mean anything to him at all that's real since yesterday?"

Luke pressed a button.

Voice of the director: "We are continuing with the same vigour as before.
And if we now get more headwind from the companies, good!
Then we'll make more noise.
We can do that.
We can do the same with American companies.
They think they have immunity under the President's watchful eye.
Fuck it!
They'll see what it means to mess with the NSA."

Loud buzzing.

Kevin made a gesture.
The murmuring stopped.

Kevin: "One more thing.
We have one more." He took a break and looked into the room.
"And that is what we are most concerned about.
The director is talking about Marlene."

Luke pushed again.

Director's voice: "I'll take care of that little bitch myself.
An 18-year-old girl with no school leaving certificate.
She got that going and she's still on the loose.
And the CIA wimps can't find them.
I only hear Northern Italy, Croatia, Montenegro.
We're on a country level.
I want house numbers.
I'll put her on the death list this afternoon, high up.
This is overdue.
There are far less dangerous people on it.
And I don't care what all the fuss is about.
When it is done, the President will have to admit: is she on the side of the terrorists or on our side?"

Kevin: "This is incitement to murder.
On German soil.
And a state conspiracy at home."
He looked serious in the audience.
"I now expect, and I mean it seriously, an investigation and then an indictment by the Federal Prosecutor's Office against the NSA Director and the BND President, because he has done nothing.
Marlene is a German citizen.
The director must be arrested the next time he sets foot on German soil."

Lukas: "The recording of the whole conversation is on our website and also a transcript of it.
It's about three times as long as the clips you heard."

Reporter: "Where did you get this?"

Kevin: "Unfortunately we can't say anything about the source at this point.
Just this much: She works for the BND and is already in contact with the Courage Foundation.
Your legal defense is being prepared.
I don't know where she is right now."

Sophie got up.
She looked into the audience and waited a short time.
Then she said in a calm, serious tone: "Actually, I actually hope that we don't need this any more.
The source should not have to defend itself.
She took a chance.
It's a personal statement that she leaked, but it concerns all of us.
It uncovers crimes.
It is a drawing of a broken judicial system, when someone can make death threats and get away with it, and someone who brings this to the public is prosecuted.
But there are probably still such corrupt prosecutors who pursue political goals and use their independence from other state agencies to do so.
Just like Marlene Ny in Sweden did with Julian Assange for many years.
She could have revoked the arrest warrant for Julian at any time.
She didn't do it.
Even when the UN Human Rights Council said that it was illegal to detain Julian, she did not take it back.
Only when it was too late did she do it.
And in the meantime she has retired quite normally, without ever having to answer for it.
I hope that we in Germany have moved on in the meantime."

Kevin: "Yes, I hope so, too."

Amelie in the back row into the camera: "Yes, we all hope so here.
I want Marlene here to celebrate with us.
We all have to look at what's going on with the director of NSA.
If we look at this and make a fuss from time to time, then something will move.
That's why I have a new action for you.
And you can get that on a lot of YouTube channels today.
I want us all to go out tonight at 8 o'clock, at 8 sharp, onto the balcony, the street or the roof garden and shout as loudly as you can!
And if you can find a megaphone, use that.
We have some great boxes on the street outside the studio.
Everybody's allowed to call through there tonight.
Come on over!
Or roar from wherever you are!"

Oskar turned around, looked at Amelie and raised his thumb.
He shouted to her: "I'll tell you about that in our broadcast here.
It will be a celebration tonight in Berlin and other cities."
