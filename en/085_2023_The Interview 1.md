## The interview (1)
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Heinz and Frederik</span> were sitting in very casual clothes on the sofa in front of the TV at Heinz' home, each holding a bottle of beer in his hand.
On TV, an empty talk show stage was on the news.

Achmed lapped himself on the roof terrace of the Eel Garden in an extra-wide wicker chair.
He looked at a huge screen on which football matches were usually shown.
In one hand he held a cocktail with a straw and in the other a remote control.

Hans sat with his wife in his house in Tasmania.
Both ate popcorn with homemade syrup and chips with curry mayonnaise, looking at the sun just rising.

The classroom in Berlin was packed.
Many hacker friends and family members had come.
Some stood on the wall, some knelt, all of them watching a giant television.

The NSA director sat in the back seat of a limousine and saw the picture of the presenter in front of him on a screen.
He shut down.

The presenter came on stage and walked forward to the audience.
Cheers.

Hostess: "A very good evening to you... good evening... And welcome to Heike's Talk.
... Today with a super super special guest..."

Let's hear it.

Moderator: "You know it, you know it, you know it!"
She laughed.
"And she's actually there!
Yes, she has come!"

She nodded into the audience, then went to the discussion table in the middle of the stage and dropped into one of the three chairs: "Okay.
Then we won't wait long.
May I welcome... The President of the United States of America!"

She made an arm movement towards a gate in the middle of the stage.

Applause broke out, mixed with occasional booing calls, which were commented on by the presenter with a brief frown.
Bright light shone on the gate and the president stepped out.
The applause grew louder.
She walked to the edge of the stage, nodded friendly to the audience, smiled, waved.
Individual spectators got up from their seats.
The president ordered them to sit down with their hands up.

President: "Thank you.
Thank you...
Thank you for this welcome." She put her hand on her heart and smiled.
"Thank you.

Then she went to the table and sat down.

Moderator: "Madam President!
How are you feeling?"

The president laughed: "A little tired.
The trip here, so many conversations all day long and this morning I fired the director of the NSA.
You don't always do things like that all in one day."

Moderator: "Really fired?
I mean, I know the message.
Why didn't you give him the chance to go himself?"

President: "He had them.
He had a week.
And has made his position clear."

The moderator nodded.
"Uh-huh.
From our point of view here in Germany this looks a little bit like an inner power struggle or family war in the USA."

The president shook her head: "This is not war.
And no power struggle either.
The roles in our political system are clearly assigned.
And there are sometimes people who question this, but that is not the decisive factor here.
But we had a family argument, you could say that.
It happens in the best families.
And you know, I actually think it's good that this happened."

Hostess: "Good?"

President: "Yes.
An argument is like a thunderstorm.
It can thunder and lightning quite menacingly, but afterwards, when it is over, the air is cleaner, and then the sun comes out again sometime.
The vast majority of people in America have now better understood what we want. Many things are clearer."

Hostess: "Which is clearer now?"

President: "We do not want a surveillance state that persecutes all citizens right into their private lives.
We never wanted this.
We Americans have something of an allergy to surveillance.
This is enshrined in our constitution from the outset.
What is clearer to us now is that we do not want to have the instruments to make a surveillance state possible either.

To clear up a misunderstanding: normal citizens are not monitored here and never have been.
A lot, a lot of things have been recorded, but nobody is watching this.
99.9 percent of this is not used at all.
However, we also have people around the government who dream of things that come very close to a surveillance state.
And there's one less of them there now.

We also do not want to send death squads around the world to work off any lists to which a head of intelligence can add someone at his own discretion.
This can only happen with the will of the President.

What we also want is to be more precise.
Don't get me wrong.
We must protect ourselves against terrorism.
We must not relax our efforts on this point.
Terrorism has not weakened in recent years.
He's gotten stronger, more violent.
We must continue to take decisive action against this.
But we need to rethink the way we fight him."

Moderator: "But there will still be death squads tracking down terrorists and, as many say, taking them out?"

President: "We are at war.
This is the reality.
For almost 20 years now.
In a war, people die.
Terrorism has declared war on us.
We have responded.
But it's a new kind of war.
It's not like here in Germany 80 years ago.
It is no longer countries fighting each other, but lifestyles, basic beliefs.
We stand for freedom, not for God-given laws.
Everyone should be able to follow his religion as he sees fit and not force it on someone else.
This struggle for lifestyles is taking place worldwide, even in the USA.
And we have to be even better prepared for this.
There is a lot of old thinking here, imagining giant armies, complex weapon systems and world diplomacy.
All this is still necessary.
There is China, North Korea, Syria, Iran... There are many countries for which these things are unfortunately still very relevant.

But there is also this new development where all these things no longer work.
And we are not yet well enough prepared for this.
We have tried to solve this with small, fast military units, with more flexible weapons and by controlling the Internet.
And in our attempt to control the Internet, we have clearly overshot the mark in some places.
We're going to correct that now."

Applause from the audience.

Moderator: "More flexible weapons?
You mean drones?"

President: "Yes, among other things.
I know they have a bad reputation here in Germany.
But used properly, they can save many lives.
As I said, we need to be more precise.
We can no longer put innocent lives at risk.
The war is now everywhere, in every country on earth, fighters and civilians can hardly be distinguished.
Precise information about the conditions on site is becoming increasingly important.
We have always known that our information must be improved.
We have made great efforts to this end.
That is one reason why the secret services have done so much in this direction.
You were assigned... they _have_ the mission to improve our information base.

But they got it mixed up.
You have confused 'do as much as possible' with 'do as much as possible'.
And that's what we're gonna work on now: That our information becomes better, more accurate, and that innocent people no longer fall into this pattern, that our computers forget as quickly as possible all information about people who do not threaten our way of life.

Silence in the audience.

Moderator: "I was just thinking about whether or not to applaud.
He didn't come, although we don't want to be monitored here in Germany either and the majority of us would actually be happy if personal data from us were not stored in huge data storage facilities in the USA.
We also don't like these vulnerabilities that make our computers and smartphones insecure, and which our and your secret services have really built into everything.
And which have been installed again and again.
We've heard too much about this.
Are you serious now about not monitoring normal people like me and everyone else in this room?"

President: "Yes, we are now getting serious!
I don't know if that applies to everyone in this room," she smiled, "you never know who is around you, but I'll take it for granted.
I understand the skepticism well.
That's why I want action.
That is why I have issued the amnesty for whistleblowers and journalists.
And we will soon have laws that explicitly allow the reporting of weaknesses even if they violate company secrets.
We're just discussing how we can ensure that such a publication really only mentions the weak points and not other company secrets, which would not have to be revealed in order to do so.
It's not easy.
But we're making progress."

Moderator: "But if you hack into other people's computers in order to uncover such weaknesses, then that is still not allowed, is it?
That was the case with Marlene Farras.

President: "Breaking into other people's computers without permission is directly against the privacy of people or companies.
These computers belong to someone.
This someone has the right to decide what happens to it, even if it is a company or an authority.
We can't generally allow that."
She shook her head and smiled.
"But, if my plan works, if employees in companies and government agencies report all vulnerabilities that are built into programs, then no one will have to hack to find out.
That would be my solution.
I'm thinking of awarding a state prize to people who report weaknesses.
It will be highly remunerated.
And will apply to non-Americans as well."

Moderator: "But without the courageous act of Mrs Farras, the whole thing would never have gotten off the ground in the first place!
Couldn't this be done in such a way that until the time when all vulnerabilities are reported by employees, it is allowed ... or acceptable ... or maybe I say, you can squeeze an eye or two when someone gets such data about a hack?

President: "I am generally against the use of violence.
And this is computer violence.
All violence should come from the state.
I want a solution that is geared towards understanding.
We must all understand that weaknesses limit our freedom.
After all, they can always be exploited by others, not only by those who install them.
That's why I would prefer a culture of togetherness here, in which we as a community decide together: We don't want that.
We didn't want thefts, we don't want murders, and we don't want intentionally built in weaknesses.
I imagine the FBI will get their own department to enforce this.
It's actually under discussion."

Applause from the audience.

The presenter breathed in: "I think that at this point what makes our show so special about Heike's talk is a good fit.
You know about that?"

The President nodded.

Host: "We have a surprise guest in every show.
He always has a personal relationship with the guest and he is allowed to ask three questions.
In this context, I would like to take a leak at this point." She smiled at the audience.
"I'm a little embarrassed.
But we checked with your staff beforehand to see if our surprise guest is okay for you.
We don't usually do that.
But in this case, we did.
So I assume that the guest we suggested is not entirely unknown to you, is he?"

The president shook her head and smiled.

Moderator: "We had two candidates, both of whom had already had intensive contact, or let's say arguments, with American presidents, one of them also directly with you.
Both would fit perfectly for this topic: Julian Assange and Edward Snowden.
We had decided on Julian.
You're probably expecting Julian Assange to be behind this gate...
He's there too." She took a break.

"But, something happened two days ago that upset our plans.
We got a call from a young man in Italy.
He suggested another surprise guest and assured us that he would come if he was invited.
We thought it was a joke at first, but it turned out to be true.
And it's not a him, it's a her.
Perhaps you can guess who I mean?"

The president shook her head in a slight confusion.

From the curtain backstage Linda peeked out.
She cautiously but emphatically tried to make eye contact with the President.
But this one wasn't looking at her.

The moderator trembled slightly: "It's Marlene.
Marlene Farras!"

A murmur went through the audience.
One could see sheer astonishment in the face of the president.
She looked around her, at the presenter, at the audience, at the table, but quickly recovered.
Then she saw Linda behind the curtain.
Linda shook her head clearly.

Moderator: "She is here.
She's standing behind that gate right now.
If you don't mind, I'd like to invite them in.
As a real surprise guest.
Otherwise Julian Assange will come.
It's just in case you refuse to talk to Marlene Farras."

Linda started waving her arms and thought: "No, no, no way!
Don't do that!" The audience had become spookily quiet.

The president nodded: "In any case, I want that!
I mean, she's got a hell of a nerve coming here.
I can get the courage to talk to her."

Applause roared in the audience, it became louder.

Moderator, loud: "Then, welcome, MARLENE ... FARRAS!"

The light went on, the gate opened and Marlene came in.

The audience cheered, the clapping became louder and louder and everyone stood up almost simultaneously.
Everyone was looking at Marlene as she came forward to the stage.
They clapped, cheered.
Individuals shouted "Marlene!"

She stopped at the front of the stage.
The audience did not fade away.

A man shouted, "Marlene, stay with us."

She nodded slightly.
A tear ran down her cheek.
She stood there, 10 seconds, 20 seconds, an eternity.
Then she wiped the tears from her face, nodded back into the audience and went to the table.
The president had stood up and they both shook hands.
Both had a little trouble looking into each other's eyes, but after a little while it worked out.
They sat down.

