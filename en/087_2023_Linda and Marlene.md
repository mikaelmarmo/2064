## Linda and Marlene
<div class="_202x">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">Linda expected Marlene</span> behind the curtain. Next to her stood Robert.

She reached out her hand to her: "Linda Meyer".
She looked at them earnestly.

Robert: "Robert Foster" They shook hands.

Linda: "I would be exaggerating if I said: 'It is a pleasure to meet you'.
There's already been a lot of excitement because of you.
And this will just be the beginning.
But all right, we'll make the best of it."

Marlene nodded: "I can well imagine that this will make waves."

From behind Julian Assange pushed himself to the group and put his hand on Marlene's shoulder.
She turned to him.

Julian: "Hi.
Great stuff.
You've got this under control.
I can go home now with peace of mind.
Keep your tail up.
It's only the USA..."

He nodded in the direction of Linda and smiled at her.
To Marlene: "Nah, really, keep going: This is real Cypherpunk."

Marlene laughed at him and they hugged each other for a long time.

Marlene: "Thank you, for everything!
And sorry I stole your thunder today."

Julian grinned impishly.
"Ciao! He turned and left.

Linda: "Okay.
Here's what we're gonna do. You're coming with us tomorrow morning on the President's plane to Washington."

Marlene looked at her in amazement, "How? Really?" She breathed out.

Linda: "There is no other way.
I know you have your own plan, but we don't want to risk it.
In Germany you are officially still wanted with an arrest warrant, here you are known nationwide.
This puts the authorities in a tough spot.
In the USA, perhaps one percent of the people know you.
We can chalk that up to a whistleblower's amnesty.
Nor is it suitable for the masses to make a scandalous report now, about a president who brings a hacker to Washington.
It's not a good time.
That's out of the news flow for a few weeks.
Maybe a few right-wing fringe groups, but nothing significant.
We'll have the prosecution suspend the warrant tomorrow and then you and your brother can fly back home free.

Marlene: "If Amelie gets to fly with her team, that's fine with me."

Linda: "Amelie?"

Robert: "A blogger.
Quite familiar around here."

Linda was thinking, looking at Robert. He shrugged his shoulders.

Linda breathed out and then breathed back in.

Marlene smiled, Linda did not.

Robert: "We see the footage before anything goes out..."

Linda: "This is how we do it.
We'll contact the team.
Then that's a yes?"
She looked at Marlene.

Marlene: "Yes."

Linda: "But you can only tweet about it on the plane.
We'll start tomorrow at 7:30.
You will be picked up by our security service.
Until then, you can stay here in any room, I understand.
Robert will show them."

Marlene nodded and Robert took her away.
