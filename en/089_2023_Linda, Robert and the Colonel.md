## Linda, Robert and the Colonel
<div class="_202y">
    <span class="year">2023</span>
    <hr class="hr" />
</div>

<span class="first-words">The Colonel stood up,</span> propped both hands on the conference table and looked threateningly at Linda and Robert, who were sitting opposite him. Linda looked very concentrated.

Colonel loud: "Release MARWIN FARRAS"?
Then he suddenly hit the table with his flat hand.
Colonel: "YOU'RE COMPLETE!
You guys are from Washington and you think you can do things better here at Ground Zero than we can.
Don't you get it? We're dealing with the most dangerous people in the world.
If we make mistakes, people die!
You can't just have an idea from some armchair without breaking things.

Marwin Farras will not leave my house today unless I die!"
He snorted.
"It's probably just another one of his deposits anyway.
He's done things here, I'm telling you.
One of my men quit the service because of him.
He's writing parking tickets in New York now.
I bet some of his friends whispered something in your ear and you fell for it.
I say you won't get him for at least three weeks.
If he is no longer a threat by then."

Robert stood up and shouted at him from the other side of the table: "MIR REICHT'S NOW!
We don't have forever here!
Read what is written on the sheet here, then sign it!
That's your boss, damn it!" He slid the hand toward him.

The colonel pushed it away and shook his head, his face had turned red-violet in the meantime.
He got up.
"He should come and tell me himself.

Robert: "This is an official instruction, it is not a request!
How can you block yourself against it?
What are we talking about here?
I don't believe it.
Is this military or kindergarten?"

Linda: "CIA."

Robert: "CIA.
Never mind.
Kindergarten." He took a seat.

Linda calmly: "Colonel, they are not so wrong with the personal conversation.
It's really a big deal.
She's kicking up quite a bit of dust.
It is necessary that we get it today.
And that's why her boss won't be coming later, but her boss, the President of the United States."

Colonel: "WHAT?" He looked around wildly.
"WHAT?
WHAT?
WHEN?
This is a joke?"

Linda: "In six hours."

The colonel sat down again and shook his head.
"Here?
The president?
Probably with press.
No, you can't do that.
You can't do that."
He looked around the room.
"Oh, you guys are... REAL MOTHERFUCKERS!"

Robert jumped up, clenched his fist and walked towards the colonel.
He jumped towards him in fighting posture and looked at him wildly threatening.

Linda hurried around the table and went in between.
"We'll stay calm now.
All of them.
We don't have much time left."
She took them both in her arms.

Everyone sat back down at the table.

Linda: "What's the problem with early release?"

The colonel closed his eyes and said quietly, "What is the problem?
What's the problem?"

He hissed: "The problem is that you have no idea how this is going on.
The problem is information.
The boys and girls who have been here for a few years have seen a lot, witnessed a lot.
Tactics, techniques.
It's all super secret around here.
They know people here.
Memorizing faces.
I don't want to see them in any sketches on the Internet."

Linda nodded, "Well, then why are we doing it in three weeks?"

Colonel: "For this we have a special treatment "dismissal".
People need to know that the only way they can get out is to cooperate with us.
Or to put it another way: you get something like life-long probation.
It's about cooperative, constructive behavior.
You can also say respect or fear.
If they come out and start performing like King Kong, that's a problem.
If they behave cautiously, strangely or contradictorily, it helps us more, it generates respect for our work.
We want to have a reputation for changing people.
We lay off about 10 to 15 percent this way, for that purpose, intentionally."

Linda: "Okay.
Then we'll just have to cut it short.
Six hours left."

The colonel exhaled: "That's not enough.
You have no idea.
With Farras, three weeks is still too short.
Did you even look at his file?"

Robert sat up threateningly.
Linda made a soothing gesture to him.
"Colonel, we know this goes against their normal procedure, perhaps even against their experience.
But we're also working at ground zero.
We often have to get people to do something you never thought they would do before.
Sometimes in hours.
It's a good part of my job.
And I think I can make this work."

Colonel, acting curious, "What do you want to do?"

Linda: "I want to get him to sign something, a story that is bearable for me, for you and a little bit for him.
He's coming out.
He'll be surprised.
He wants out.
He'll meet us halfway."

Colonel: "Sign?
This is a terrorist...
What does he care about his signature?"

Linda: "You may then also threaten him a little.
In the end he will know that his signature is taken seriously.
You have experience in that."

The colonel frowned: "This will be a super-GAU, I tell you.
We'll be all over the papers in a few weeks.
And then we sit here together again and think about how it could have come to this."

Linda: "He is not out of this world.
We're not gonna lose him.
And press is our business.
We're gonna do a lot to keep that from washing up on the surface.
He becomes part of a blackout campaign, we make his life a little less visible.
In four or five months, nobody will know him.
What's up with him?
A prison story.
There's a lot of them out there.
And people say..."

The colonel interrupted: "... we already know, we already know and look away.
I know.
The masses want news, they want to be seduced.
But I have a bad feeling about this.
The guy, I'm telling you, Farras, he's..."

Linda: "The guy will be forgotten.
I'll take care of that.
And you'll get a promotion for this one.
From the highest authority."

Colonel: "What kind?"

Linda: "I cannot say that now. But one that they can't and won't turn down."

The colonel leaned back and said, "Uh-huh."

Linda: "Come on, let's get to the text, and then we'll go visit this guy."



About an hour later Linda tidied up the papers and said to Robert: "Come on, let's go!"
Then to the colonel: "You wait here.
Let's warm him up a little first."

They got up and walked through several corridors to the interrogation room.
Marwin was already sitting there at the table, his back turned to them.
Linda and Marwin walked past him and sat down opposite him.

Linda: "Marwin Farras?" She gave him a friendly nod.
