## Attack from the Internet
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Lilly stood slightly dreamy</span> at her high desk in her team room.
Her fingers flew across the keyboard as she moved back and forth between two monitors in front of her.
Morpheus put his hand on her shoulder.

Morpheus: "And do you find anything?"

Lilly shook her head slightly.

Morpheus: "Perhaps he has given up.
I mean, as big as he'd put that on, he can't think he'd get another chance to replicate it:
He had four shadow accounts, manipulated the user manager to stop reporting suspicious movements and so on ..."

Lilly was silent and typing.

Morpheus: "It's been three weeks and we haven't found anything new on him."

Lilly: "Maybe he's waiting until we stop looking."

Morpheus: "How could he know that we are still looking for him?
And how will he know we'll stop looking?
Come on, we've got a lot to do in the next few days.
The Cypherpunk Academy gets level island 6 in addition.
Three players are already on level 4.
Your brother Lasse, for example."

Lilly: "Yes, I also have to do something about it.
But this is bugging me.
It's illogical.
I don't know how the guy even got into our system.
I just don't understand how he got started.
You gotta have a foot in the door to do something like that."

Morpheus shrugged his shoulders.

Morpheus: "You will not always understand everything that goes on in our system."

Lilly gave him a sharp look.

At this moment, all the large monitors switched their picture and showed a red bar above them.

Morpheus: "These are my consoles!
They show my consoles!"

He ran to his table and gave some orders.

Morpheus loudly: "We have a DDoS attack!
Cypherpunk Academy is down to 8% availability... 7 ... 5 ... 4 ... all gone.
Currently, 3.2 million different servers access the Academy, which mainly request four different services".

<div class="infobox" id="ddos"></div>

Lilly shouted: "2.9 million are hostile, 300 000 normal users.
I'm trying to isolate them...
They shoot like stupid...
They change their strategy...
I can't keep them away!"

Morpheus: "Now more are coming!!
Shiiiit!
5 million... 6 ... 8 ... 13 ...
It doesn't stop."

From another table: "I'm taking the Academy off the grid!"

Morpheus: "No!
We have thousands of battles open, dialogues... 300 are currently trying to solve thought error puzzles.
You can't break that off in the middle of a game.
We have to protect the data.
And we want to see his strategy."

Lilly: "He changes them all the time.
If I divert, he'll respond immediately.
How does he do it?
I'll grab one of the servers and see what's on the other side."

Lilly sent a message to Marlene on the side:

<div class="terminal">
Marlene! Come to my squad room.
We just got a very strange DDoS.
</div>

Morpheus: "What do you want to do?
They're coming from the gate grid.
You can't reach it."

Lilly took a stick out of her pocket, showed it to Morpheus and then plugged it into the computer.

Lilly: "14 million different response packets, each trying to exploit a vulnerability in its own way.
We have just over 14 million servers trying to get into the Cypherpunk Academy.
I'm gonna send them after the attacks.
Let's see what happens..."

Morpheus: "14 million, where did you get that?"

Lilly typed at her speed, as if lost in another world.

From another table: "Oh, that's causing problems for the attackers.
They slow down 20 percent."

Lilly: "I don't care about slower.
I want just one or two of what I send back to them."

Everybody looked spellbound at the large monitors.
The number of attackers did not decrease.

Morpheus: "How can you tell if a server is down?"

Lilly: "Wait..."

Morpheus: "They change their Internet address, so you can't see if an attacker won't come because he crashed."

Lilly: "Wait..."

Morpheus: "They also send irregular..."

Marlene entered the room and stood beside Lilly.
They nodded their heads briefly.

Lilly shouted, "DA!"

She switched the big monitors to hers.

Lilly: "This is from an assailant.
This is the attacker's source code."

Everybody looked at the monitors with concentration.

Lilly suddenly looked up in amazement, then at Marlene, who gave her an equally questioning look back.
Then both looked at Morpheus together.

Morpheus was terrified.

