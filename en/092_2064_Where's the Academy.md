## Where's the academy?
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Tim's house was situated</span> in a quiet American suburb.
It had a beautiful white façade with two white wooden columns to the right and left of the entrance, a well-kept garden all around and a large garage.
The garage door was wide open.
From inside, a loud, bright engine noise could be heard.

Suddenly a huge off-road motorcycle shot out of the garage door with a loud howl.
Tim and Sigur sat on him with their helmets on.
Tim braked sharply on the road so that the rear tyre slipped to the side.
Then he stepped on the gas.
The front wheel immediately lifted up and Sigur gripped tighter around Tim's stomach.

Sigur screamed: "Woaaaahhh.
It's a machine!
I love it!"

Tim: "140 horsepower.
But wait till we get cross-country.
That's my topping.
I won the West Coast cross-country title two years ago."

Sigur: "I heard that. Super awesome!"

Tim braked again abruptly and let the machine slip, accelerated, the wheels spun.
They were headed for a vacant lot.
Behind it there was a steppe landscape and mountains on the horizon.
At that moment two black SUVs came from the right and a dark blue one from the left.
Sigur looked to both sides.

Sigur: "Hey, this is not good.
Where did they come from all of a sudden?"

Tim: "Whatever.
They won't catch us.
The steppe is my living room.
I learned to drive here.
I know every tuft of grass here."
He sped up.
They drove into open country.
Further back, a country road could be seen coming out of the city and getting lost somewhere in the mountains.

Sigur pointed in the direction of the country road and shouted: "Shiiit.
Ten SUVs.
Why do they all have the same cars?
And more back there."

Tim: "They have found us!
But they haven't got us yet.
I've always wanted to play Jason Bourne.
The car chases are so awesome there."

Sigur: "Not really me!"

Tim: "They have no chance.
They have no idea how fast we can go." He sped up once more.

About 300 metres further on, a column of five wagons had left the road and tried to cut them off.
Tim steered the machine slightly to the left.
They were breathtakingly fast.
Sigur kept shouting "Cool!" to suggest that the motorcycle ride would be some kind of fairground attraction.
But with his hands he clung to Tim's stomach in panic.
Tim drove past the car group at a distance of about 50 metres and then on to an open field.
The SUVs tried to follow behind, but stopped after a short time.

Sigur looked back: "Cool.
We lost them!" Then he looked up into the sky and said, "Shit!
Oh shit."

"What?" cried Tim.

Sigur: "Two Apache helicopters."

Tim turned briefly: "Shit!
I can't go any faster here.
But we'll be in the woods soon."

Sigur: "Oooh, God!"

In one of the helicopters, a soldier pointed his sniper rifle at them and spoke into his headset: "Roger.
Got passenger green.
Request permission to fire."

A voice croaked over his headphones.
"Negative.
No clearance.
Hold your fire.
Shoot the motorcycle or the rider so that they fall."

Soldier: "I can catch the driver.
But the speed is too high.
50 percent chance that the passenger dies."

Voice: "Then stop.
Wait till they slow down."

Soldier: "Roger."

The motorcycle made a huge jump over an obstacle, stumbled.
The soldier aimed more precisely, but Tim was able to intercept the machine and accelerated again.
Sigur took off his helmet and threw it away.

Tim: "What the hell are you doing?"

Sigur: "They do not shoot.
They want me alive.
In the collateral murder video, they were much further away than they are now.
It wouldn't be a problem to hit it from a distance.
So I take off the helmet so that they don't get the idea to shoot at the bike.
Make sure you keep pushing the envelope."

Tim steered the bike into a dry riverbed.
The forest was still about half a kilometre away.

Sigur: "Where are we actually going?"

Tim pointed half right at a mountain range.

Tim: "There.
Behind it."

Sigur saw a huge white balloon floating above the mountain range, with a painted mosquito fly.

Sigur: "To that balloon there?"

Tim: "What balloon?"

Sigur: "The white balloon there!
Don't you see him?"

The Mosquito Fly began to flutter.

Tim: "I don't see a balloon.
They are mountains, clouds, trees.
What do you think?"

Sigur looked in surprise in that direction and quietly said to himself: "Well, this one..."
