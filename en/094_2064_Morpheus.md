## Morpheus
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Morpheus looked startled and spread his arms.</span>
Marlene and Lilly looked at him.

Morpheus: "I... um... I... um... I can explain."

Lilly: "You can explain that the academy is attacked with one of your admin keys?
You know what that means!
Each of these programs can do infinitely many things on our systems here.
14 million little admins.
How will you explain this?"

Marlene: "Yes, how?"

Morpheus sat down on one of the standing stools and looked down.
Without looking up, he said, "You're hardly using admin privileges."

Lilly snorted, "That's totally fine!"

Marlene: "If a key like this gets into the wrong hands..."

Lilly: "How can you even give an admin key to the outside?
You can't do that.
Fuck!"

Morpheus breathed out audibly.

Marlene: "Now we have to close all your entrances.
At least until this is cleared up.
The Academy administrator has to figure out what we're going to do now.
You can lose your employment as a computer administrator over something like this."

Morpheus bit his teeth and squinted his eyes.

Lilly: "I hope you lose it.
You can't give out an admin key."

Marlene looked at Morpheus: "You said that you could explain this.
Gee!
Such a risk.
Why?
We would have found out eventually.
Or did you want to destroy the Academy?"

Morpheus loudly: "NO! I didn't mean to do that.
Not at all."

Marlene: "Then what did you want?"

Lilly: "You can say that now.
I look at everything you've done here anyway.
Even if it takes months."

Morpheus: "You won't find this in the logs.
Not even in the journals."

He breathed out loud and then pointed to a laptop that was standing next to his monitors.

Lilly: "Your Tails laptop.
Fuck.
Where's the stick?"

Marlene: "And the password!"

Morpheus shook his head.

Marlene: "Morpheus!"

Lilly to Marlene: "You can't demand the password from him.
This is his."

Marlene: "He'll lose his employment rights if he doesn't show everything that's happened."

Morpheus: "I was expecting this."

Lilly and Marlene looked at him stunned.

Marlene: "You risk your employment law in a field where you have visible talent?
This is crazy.
For what?"

Lilly: "For what?"

Marlene: "The Cypherpunk Academy is a place where young people learn what they are looking for.
They like being there.
You learn programming and hacking.
You get to know the fundamental errors of our culture.
They learn to deal with their feelings.
They learn to meditate.
They learn courage and spontaneity and trust and helpfulness.
Why do you want to destroy that?
Why do you want to..."

Morpheus: "This is only the outside!
This is how you represent the academy.
That's why all the kids want to go there.
But it's not like that!
Inside, it's very different.
Maybe you don't see that.
Maybe you're too close.
But there are some people who see that.
They see what the academy does with the young people.
Sometimes even with children.
Even with my..."

Marlene: "Rebecca?
What happened to her?"

Lilly: "And what do I not see?
Where does anybody do anything to me I don't want to do?
How strong is the conspiracy theory?"

Morpheus: "Yes, my Rebecca.
She's totally changed since she wanted to join the Academy.
She's not in yet, but she's close."

Lilly: And you looked that up in her account as an admin...
You entered an account as an admin with personal interest.
Asshole!"

Morpheus: "I am not.
She told me she saw the balloon.
I do not go into private accounts!"

Lilly looked at him with her mouth open.

Marlene: "You do not go into private accounts.
Then what was this?"

Morpheus: "This is not personal, what I did.
The academy turns the heads of the young people.
You're being led into something too soon.
They must decide for themselves whether they want to think as the Acadmie dictates.
It's like the military academies at the beginning of the century.
Own thinking?
No, thanks!
Think the way the Academy wants you to.
For example, in the case of cultural errors of thought.
Rebecca looks for them all over the game and if she finds a clue, she tries it ..."

Lilly was cooking inside: "Fuck!
I have no thinking of my own?
You don't even know me and say that.
Is that your own thinking?"

Marlene put her hand on Lilly's shoulder, "You have anger."

Lilly nodded, turned around and took a few steps towards the wall.
She stopped there and closed her eyes.

Marlene: "Which thought are you referring to?"

Morpheus looked at her in amazement.

Morpheus: "There are some dangerous ones.
But the one about not helping people who are oppressed was quite difficult at home with Rebecca and her mother.
But this is just one of many examples."

Marlene: "You mean the thought that I have a right to help when someone oppresses me?
And that instead you can think that the other person who oppresses you needs help because he or she cannot manage to be human.
And that is one of the worst things that can happen to you in life, that you lose contact with being human and seeing the other person as human."

Morpheus: "You always express that in such a way that you think it's true.
The fact is that Rebecca has repeatedly told her mother that she sees herself as a victim and is not one at all.
But if you know my wife's story...
Gee!
Nobody wants to grow up like that.
Never.
She slipped into a depression afterwards.
It's her only daughter who says that."

Marlene: "And Rebecca is to blame that this happened because she says something like that when she is 13 years old?"

Morpheus: "This is not about guilt!
I know, you're trying to tell me that my wife, as an adult, is responsible for her own reaction to what others say.
Shit!
Depression is an illness.
If Rebecca hadn't said that with the victim, over and over again, the depression wouldn't have come back up.
It's a fact!
And she's doing it because you're turning her thoughts around."

Marlene: "And that's why you wanted to shut down the Cypherpunk Academy?"

Lilly turned from the wall to the two of them and said dryly: "No, he wanted to leech the cultural thought errors.
He wanted to hack it and publish it.
That they can see everyone, not just those who play the game."

Morpheus: "Yes.
Right!
You always act so hypocritical that everything should be open.
After six weeks at the latest...
And then you do this shit with the mind mistakes.
Everybody's looking for it like crazy.
Nobody knows what's in there.
I only hear about Rebecca from a few people by chance and one of them is so twisted that my wife gets sick from it.
We have to get them all out in the open.
Everyone has a right to see them and to form an opinion for themselves.
Don't you?"

Lilly closed her eyes and shook her head.

Lilly softly: "And because we have a whistleblower amnesty, you thought you'd just get out of it."

Marlene: "Morpheus!
What you want isn't possible.
You can't get the academy's mental defects."

Morpheus: "That's exactly what the Academy administration told me when I wanted to see them a few weeks ago.
You only get them from players, in the game, they said.
And here in the database everything is encrypted, with player keys.
It's classified.
It's against our ethics."

Marlene: "No!
It's not against our ethics.
Everyone has his own version.
You have to put them together.
There is no official list of thought errors.
These are only made visible through the game, individually for everyone different.
But you can have mine if you want.
I can send them to you.
But these are mine.
Not the academy's.
Or any general.
That wouldn't work at all.
A general description of our cultural thought errors?
That would be the biggest nonsense.
Until we had a few hundred players, the mistakes of thought were not worth much."

Morpheus softly says, "There is no list at all?"

Lilly: "Of course not!"

Marlene: "Everybody has to take a stand for themselves and the game and the reactions of the other players will show you how your version of the thought error helps others.
You can even add new ones sometime."

Morpheus: "Fuck!
I did not know that."

Lilly: "Fuck! Yes!"

Morpheus: "And which one got Rebecca then?"

Marlene: "You have to ask her that."

Morpheus: "But how do I prevent Rebecca from getting one of these?"

Marlene: "She wants to learn how the world really is.
What are you trying to prevent?
That she has experiences?
You can tell her that she can come to you at any time if she experiences something that upsets her.
But knowing Rebecca as I do, she knows at least three adults to whom they come with everything at any time ...
And I also don't see that she has a hard time with what she picks up in TRON.
Your wife is having a hard time."

Morpheus: "Yes, because Rebecca..."

Marlene: "... has created feelings in her?
Morpheus?
Really?
A Taurus shirt can create feelings in you.
At least indirectly.
But not another human being."

Morpheus: "Yes, I know that.
But this is different.
I myself can usually leave out what others bring to me.
And the rest I can meditate with.
But my wife can't.
I have to take my... I want my wife as she is.
And that doesn't grab her when someone on the outside says that she thinks she is a victim, but is not one at all.
Especially when Rebecca does it."

Marlene: "Then help her.
I think she could use some help.
Rebecca's doing just fine."

Morpheus: "Fuck.
That always sounds so nice.
But I have... I just feel..."

Marlene looked at him, "You're scared."

Morpheus closed his eyes and nodded.

Marlene: "And fear is really..."

Morpheus: "I know!"

Marlene: "This is one of the most dangerous errors of thought."

Morpheus: "I know.
Fuck!
Yah."

He breathed out.

Marlene, Lilly and Morpheus looked at each other for a while.

Morpheus: "Now what?"

Marlene: "Because of the alarm there is a transcript of what happened here and what we spoke.
We send it to the community.
No comment.
You hang the logs on everything you've done.
And then we'll see."

Morpheus nodded.

