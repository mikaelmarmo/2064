## Temple Puzzle
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">Lasse galloped in full knight's armor</span> with his armored horse towards the city gate.
He swung a big morning star with "Python Advanced Algorithms" at its side and shouted as loud as he could: "Attackeeee".
Around him, foot soldiers with spears, swords and shields ran towards the gate.
From the background catapults fired burning bullets into the city.
Over the whole width of the city wall you could see soldiers in armour shooting arrows, throwing stones or trying to push back soldiers climbing up ladders with lances.
Above the gate, a group tried to pour a huge bowl of hot pitch on a group of soldiers with battering rams who were trying to smash the gate.

Lasse stopped his horse abruptly about 30 meters before the gate and pointed his morning star in the direction of the gate.
A big, light blue-green-violet ball lightning came out of the top of the morning star, struck the city gate with force and exploded in a huge cloud of fire.
Parts of the gate and the city wall flew around.
When the cloud of fire dissipated, there was a large gap in the wall where the city gate had been before.
Lasse pointed with his morning star in the direction of the gap.
A second ball lightning bolt hissed and exploded in the first house behind the former city gate.
From different sides, four knights galloped towards the gate, surrounded by hundreds of foot soldiers.
Lasse gave his horse the spurs and stormed into the city with the others.

The foot soldiers climbed the city walls from inside, penetrated the houses and chased the remaining soldiers of the city.
Lasse and the four other knights gathered and looked around where their intervention seemed necessary.
There was nothing more to do, the foot soldiers would manage the rest of the work on their own.
And so they turned towards the weapons tower, which stood at the edge of the market place.
Each knight took his main weapon before him.
Lasse fired another ball lightning, another knight from a kind of trident fired a jet of ice, another threw a huge Thor hammer.
A knight raised his hands and a rain of steel chunks fell on the weapon tower, the fifth knight shot angular steel balls that caused explosions.
The weapons tower began to burn and finally exploded in a huge colourful cloud that spread over the whole city.

The knights had already turned them around and trotted towards the temple, which stood in the middle of the market place.
It was pentagonal, with a pillar entrance on each side.
The whole temple was protected by a blue cone of light which embraced it completely and seemed to reach to the sky in its ever thinning tip.
The knights jumped off their horses, took off their helmets, opened their armour and laid them on the ground.
Then each one went to one of the five sides of the temple, each in front of an entrance, still outside the light cone.
Lasse knew the temple.
He stood in front of the same entrance as last time.
Then they all stepped together into the light of the cone.
At one knight there were sparks and you could hear a threatening crackle.
Then he was thrown out with a bang.
The other four knights left the beam of light and ran to the knight lying on the ground.
He stood up and the others put their hands on his shoulders.
You could see a yellow energy stream flowing.
Then everyone went back to their position in front of their entrances.
This time everyone got through the blue light curtain without any problems.

Lasse entered the temple hall.
The hall consisted of a large dome-shaped room divided into 12 sections by double columns.
In front of each section was a massive marble table.
In the middle was a kind of round altar, with a large floating crystal ball above it, which glowed matt in various colours.
The colours moved slowly, like currents in a sea.
One of the 12 sections shone brighter than the others.
There Lasse went and stood in front of the table.
He did not see the other knights.
In magical temples you were always alone, but you could only enter five of them.
The others were here too, only in their own dimension.
Lasse knew that what they were doing could have an impact on what he was experiencing, but he had not yet figured out how.
On higher levels, one even had to work together across the different dimensions here in the temple.

But at first Lasse was only interested in solving the temple's thought error puzzle, his first ever.
He had already tried to solve this riddle twice, but had failed twice before even the actual thought error was revealed.
Otherwise he would see it in a designated field in the middle of the wall.
The place was still empty.
So far he had only managed to find the first six thought steps and put them in the right order.

Each temple in the game was responsible for a cultural thought error from the beginning of the 21st century.
These were thought mistakes that were rooted in the culture that everyone learned from a very young age and no one noticed anymore that they were thought mistakes because almost everyone thought like that.
In the age of chivalry there had been such a cultural error of thought that the earth was a flat disc surrounded by an ocean that ended somewhere in a huge waterfall.
And people who at that time already knew that the earth is a sphere and that the people on the other side are practically upside down were considered crazy.

Lasse took an orange stick that lay on the table in front of him.
In the six sections to the left of his, six sentences appeared in orange, slightly luminous script.
He turned to the first movement.

<div class="thinking-mistake-orange">
(1) The way we live together in society is increasingly marked by a kind of paternalism.
We want to change the people who do not behave "right".
We have less and less confidence in the individual.
The state must use its power to provide security,
Politicians should always make better laws and regulations,
which are for security and to which all should submit.
</div>

Lasse thought: "That sounds like fear of losing control.
The others are unpredictable and that's why you need rules.
Uh-huh.
I can think that way."

He turned one section to the right.

<div class="thinking-mistake-orange">
(2) This is so because man,
even if he is born,
has a natural selfishness,
who needs to be put in his place,
that you can't just let go,
without ending up in chaos and uncertainty.
</div>

Lasse: "Yes, a world that is freely left to children ends in chaos.
Okay.
But are children selfish?
Not until they are conscious of themselves, definitely.
Then they cannot yet relate to the world.
But you can see it in the way that children are selfish."

He went on to the next section.

<div class="thinking-mistake-orange">
(3) Of course, this remains the real goal,
that everyone gets as much freedom as possible.
But man can be free only in security,
when he feels safe.
</div>

Lasse: "As much freedom as possible ...
That sounds dangerous.
And then freedom is tied to a feeling... that you feel safe.
Twice as dangerous.
Hmm.
But understandable.
You can think that."

<div class="thinking-mistake-orange">
(4) There is certainly a deeper meaning to it,
that the individual is protected by law and justice,
but when people are punished because of that,
who don't play by the rules,
...will end up in a system of threat and fear.
And as humans, we do not want to live in threat and fear.
</div>

Lasse: "Exactly!
We need laws and we must protect them.
That's right.
But not by punishment.
That was the main mistake here.
If someone screws up, I punish him so severely that the next time he decides not to do it anymore.
And others, who see that he is being punished, do it right from the start.
But then they do this not because they understand what is right and what is crap, but because they submit.
People do not submit well.
This does not work in the long run.
But if you punish it, it only works for a short time and then the boomerang comes."

<div class="thinking-mistake-orange">
(5) People with whom we live together in trust and openness,
we don't want to use punishment, education, or other measures to make it so,
to behave "correctly".
We'd rather trust that.
But when someone breaks the law in public life,
threatens the state or other people,
it's important to us to get him to do that,
to return to positive behavior or at least to abide by the law.
</div>

Lasse: "Yes, yes ...
It's hard to believe that you can trust someone you don't know.
Then don't trust me.
How can you even trust people you don't know?
Difficult question...
... on the other hand, you only have to look at the open source movement.
It's not about trusting someone not to screw up, it's about trusting that if they screw up, we can handle it.
Linux has experienced millions of bugs, and yet it is the most stable operating system of all.

<div class="thinking-mistake-orange">
(6) If we believe,
that someone is acting destructively,
because he's sick,
then that's the goal,
that he's cured.
He should be given time for that.
But if he's not considered sick,
if he chooses to do so of his own free will,
to do destructive things,
then he must be forced to do so permanently through threat and control,
to abide by the law.
</div>

Lasse: "Of my own free will...
When do I want to be destructive?
If I consider anything to be nonsensical,
what I'm supposed to do,
when I don't see any other way out,
when I get angry and I don't know where to put it...
But then I don't want to do evil simply as an end in itself,
I want something to stop,
I want to get rid of a threat.
Unless I'm the wild card.
But he really is mentally ill."

He turned to the free sections, where one sentence appeared in white in each case.

Lasse read it.
Turned from one to the other.
And suddenly said, "YES!"
He went to one of the sections, stood in front of the table and said aloud, "You're the next step."

The white sentences disappeared and the sentence in front of him became orange.

<div class="thinking-mistake-orange">
But the price of that education,
for wanting to change the other person,
is high:
Thus there is no autonomous freedom in social life,
no freedom based on trust, where everyone decides for themselves,
what he can and cannot do.
And this is how we want to live.
The fact that it did not happen is due to a cultural error of thought:
</div>

Lasse clenched one fist: "YESSSS!"

He breathed deeply.
That was the seventh step.
He turned to the crystal ball in the middle of the room.
It began to glow and the colours moved faster.
A beam appeared, shining out of the sphere into the room and moving to the section Lasse had been standing in front of at the beginning.
The beam moved to the free field in the middle of the section and started to draw a green text.

<div class="thinking-mistake-green">
Man acts selfishly because he is born selfish.
</div>

Then the beam changed its question to blue and wrote underneath:

<div class="thinking-mistake-blue">
Man acts selfishly because he ends up in fear.
</div>

Lasse: "YES!"
That was his first exposed thought error.
That was the first half of what was to be done here in the temple.
The second was to solve in.
The corrected thought appeared of its own accord.
What was missing was the train of thought to understand the corrected thought.

The orange texts in the other sections had disappeared.
Lasse put the orange stick on the table and took the blue one for the first time.

Three blue texts appeared in three of the sections.

<div class="thinking-mistake-blue">
And that depends above all on it,
if we had enough adults around us when we were kids,
that we could copy from,
how a good autopilot system works,
Adults who were paying attention,
...have supported us,
have shared their experiences with us,
in a familiar, secure atmosphere.
Or whether this atmosphere was already characterized by fear and autopilot reactions back then.
</div>

<div class="thinking-mistake-blue">
Inner confusion, despair, unprocessed shock experiences,
Ideas that do not correspond to reality create fear in various situations in life.
</div>

<div class="thinking-mistake-blue">
Such a form of freedom therefore works wonderfully,
because we are fully prepared for it,
because we all have some primal experiences at the beginning of our lives,
the free give and take,
the confidence in ourselves and in our environment.
</div>

Lasse looked at her and said after a while: "Sure!
The beginning is always easy."

At that moment Mostafa appeared.
He fluttered around in the dome a little until Lasse opened his flat hand.

Mostafa landed and said: "End of game!
Cocoa at Cypherpunk Cafe."

Lasse: "Let me solve it quickly.
I know the first blue sentence."

Mostafa: "Can you do tomorrow.
Everything is stored here."

Lasse: "Saved?"

Mostafa: "Yes, of course, once you have revealed the thought, you can come here any time.
You are now a candidate here at the temple."

Lasse: "Cool.
Okay.
Let's go."

Mostafa: "Think about the cafe."

Lasse: "Ok!"

In a flash the temple disappeared and Lasse sat at a cafe table under the open sky at the top of the Cypherpunk Academy.
He marvelled at the view.
The nearest mountains were certainly 30 kilometres away.

The place of Sigur opposite him was still free.
Lasse asked in the direction of the free chair: "Well, where are you right now?"
