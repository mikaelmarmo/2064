## The Cypherpunk Cafe
<div class="_2064">
    <span class="year">2064</span>
    <hr class="hr" />
</div>

<span class="first-words">The Cypherpunk-Cafe was located at the top of the balloon, under the open sky.
It looked a little like an Italian cafe terrace, with marble floors and columns, soft colours, small tables and a stone wall all around.
Behind it one saw mountains, a big lake, a whole miniature landscape.

Sigur sat down with Lasse.
There was already a big glass with an ice-shake in front of him.
Next to it was a thick, leather-covered book, which he did not pay attention to.
Wim, the bear sat a few metres away from them on the stone wall of the cafe with his legs dangling outside.
Wunderlich chatted animatedly with Mostafa at the neighboring table, both in front of him a large, elaborate cocktail cup with several straws and many colorful glittering ribbons.
Wunderlich pointed with his thumb to Sigur from time to time, said something and grinned at Mostafa.

Sigur looked around: "Boa, who's all here!
That's crazy... So many Cypher Punks in one place... Hey, Totaot Two!" He beckoned to a table on the other side.
Totaot2 waved back.

Lasse: "Yes, it's nice here.
And you only see up to level four and the teachers, of course..."

Sigur: "I haven't even passed the sphinx on Level 3 yet."

Lasse: "Never mind, you'll get them tomorrow.
You're always here on the level you want to be on.
If you choose too high, then you're more often with Wunderlich.
On the levels themselves you can only do something when you have passed him and the sphinxes.
You can always come to the cafe. It's always open."

Sigur dry: "Wunderlich was extremely annoying.
The Sphinx doesn't mind."

Lasse nodded: "There are also some annoying sphinxes.
It's easy to get caught.
But strange is special.
And you can meet him in so many different places.
I once had to negotiate a money loan with him in a bank.
He was the bank manager.
I'm telling you, he drove me crazy.
I hit him!
Really.
I punched him...
Boom!
I woke up in the real world in the middle of the night for days after..."

Wunderlich cheered over and grinned.
Then he burped loudly.

Let quietly to Sigur: "I think he can read minds.
I'm pretty sure."

Sigur: "Real! Shit..." He looked at Wunderlich.

Lasse: "The third Sphinx is good, very exciting.
She almost got me today."

Sigur: "You always have to go back to her?"

Lasse nodded: "Often.
Sometimes totally surprising.
That's refresher, exercise, stay agile.

Let's sit over at Wim's for a while.
He can give you a few more tips on Wunderlich.
Knowing you, you'll meet him again tomorrow."
He smiled at him.

Sigur moaned.
He looked up at the table and pointed to the book, "What is that?"

Lasse: "Ah, I almost forgot.
Your log.
There is a new medal in there for what we did today and the three Wunderlich globes.
Not a bad start.
It also has our new mission in it."

Sigur: "Our new mission?
What is that?"  He tried to open the book, but he couldn't.
It was like glued shut.

Let: "Just think what you want.
You're in the Cypher Punk Academy."

Sigur looked at the book and it opened on a page with a shiny medal.

Sigur: "Wow!"

Lasse: "A Steffens-Lampresa medal.
These are the two who in reality have reduced the NSA main building to rubble with stolen combat robots.
That was 36 years ago.
I got them too."

Sigur: "She looks really good!
Is that them two down there?"
He pointed to two pictures.

Lasse nodded.

Sigur: "One looks Brazilian.
Boa, and the other one has a bodybuilder's figure.
Hello."

Lasse: "They're both Brazilian."

Sigur: "Okay. Are they still alive?"

Lasse: "No ... they didn't make it."

Sigur: "Shit.
The world was really like that then. Madness."

Lasse: "They died shortly before the end of the war, Secret Service.
I'd rather live in a room with Wunderlich than work in a secret service that murders people and stuff."

Sigur exhaled. "Curious..."
He looked at the book again and it closed and opened again.
Wunderlich grinned at him on the opened side.
Sigur shook his head in shock.

Sigur: "Let's go to Wim."

The book closed. They got up, climbed the stone wall and sat down on the left and right of Wim.
He put his arms around both of them.
