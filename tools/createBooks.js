const fs = require('fs');
const YAML = require('yaml');
const {
    execSync
} = require('child_process');
const {
    paramCase
} = require('change-case');
const {
    readChapters
} = require('./common');
const chalk = require('chalk');
const {
    version,
    license,
    repository,
    isbn,
    asin
} = require('../package.json');

const defaultLanguages = ['de'];
const commander = require('commander');

const TEMP_PATH = `${__dirname}/tmp`

commander
    .version('1.0.0')
    .option('-l, --languages [lang Code]', 'Specify one or more languages [de,en]')
    .option('-t, --types ["epub","pdf"]', 'Specify the type of book, default: ["epub","pdf"]')
    .option('-b, --build [build number]', 'Pipeline build number')
    .option('-c, --commit [git commit]', 'Commit Id')
    .parse(process.argv);

const bookTypes = commander.types && commander.types.split(',') || ['epub', 'pdf'];
const languages = commander.languages && commander.languages.split(',') || defaultLanguages;

const prepareFile = async (chapter, language) => {
    const fileName = `${language}/${chapter}`;
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, {}, async (err, fileData) => {
            if (err) reject(err);
            else {
                fileData = resolveReferences(fileData, language);
                fileData = resolveVariables(fileData);
                resolve({
                    len: fileData.length,
                    fileName,
                    fileData
                });
            }
        });
    })
}

const resolveReferences = (fileData, language) => {
    let text = `${fileData}`;
    const re = /(<div.*class=".*\b(infobox)\b.*".*id="(.*)".*>)</gm;
    let ref;
    const references = [];

    while ((ref = re.exec(text)) !== null) {
        references.push({
            replace: ref[1],
            type: ref[2],
            content: ref[3]
        })
    }

    for (let i = 0; i < references.length; i++) {
        const {
            replace,
            type,
            content
        } = references[i];

        const fileName = `${__dirname}/../${language}/dictionary/${content}.yaml`;
        const yamlText = `${fs.readFileSync(fileName)}`;
        let referenceText;
        try {
            referenceText = YAML.parse(yamlText);
        } catch(error) {
            console.error(chalk.red(`Can't create YAML text from '${fileName}', because of '${error.message}'`));
            referenceText = { novel: 'There was an error reading this box.'}
        }

        switch (type) {
            case 'infobox':
                text = text.replace(replace, `${replace}${referenceText.novel}`);
        }
    }

    return text;
}

const resolveVariables = fileData => {
    const resolveData = {
        BOOK_VERSION: version,
        BITBUCKET_COMMIT: commander.commit && commander.commit.substr(0,7) || 'snapshot',
        BITBUCKET_BUILD_NUMBER: commander.build || 'snapshot',
        BOOK_REPOSITORY: repository,
        BOOK_LICENSE: license,
        BOOK_ASIN: asin,
        BOOK_ISBN: isbn
    };

    const text = `${fileData}`;
    let targetText = text;
    const re = /\$\{([^\}]+)\}/gm;
    let ref;

    while ((ref = re.exec(text)) !== null) {
        targetText = targetText.replace(ref[0], resolveData[ref[1]]);
    }

    return targetText;
}

const convertBook = (sourceFile, options, type) => {
    const { language } = options;
    const bookName = `2064_${language}`;
    const cmdOptions = Object.keys(options).map(option => `--${paramCase(option)}${options[option] !== true ? ` "${options[option]}"` : ''}`).join(' ');

    const cmd = `ebook-convert ${sourceFile} ${bookName}.${type} ${cmdOptions}`;

    console.time(bookName);
    console.log(`Converting '${bookName}' to ${type} format.`);

    try {
        execSync(cmd, {
            cwd: TEMP_PATH
        });
    } catch (err) {
        console.error(err);
        process.exit(1);
    }

    console.log(`Done. You find the book at '${TEMP_PATH}/${bookName}.${type}'`);
    console.timeEnd(`${bookName}`);

    return null;
};

const createMobi = options => {
    const { language } = options;
    const epubPath = `${TEMP_PATH}/2064_${language}.epub`;

    const cmd = `kindlegen ${epubPath}`;

    console.log(`Converting '${epubPath}' to kindle format.`);

    try {
        execSync(cmd, {
            cwd: TEMP_PATH
        });
    } catch (err) {
        console.error(err);
        process.exit(1);
    }

    console.log(`Done. You find the book at '${TEMP_PATH}/2064_${language}.mobi'`);

    return null;
};

(async () => {
    for (language of languages) {
        const chapters = await readChapters(`${__dirname}/../${language}`);
        const bookData = await Promise.all(chapters.map(chapter => prepareFile(chapter, language)));
        const bookText = bookData.reduce((acc, data) => {
            return `${acc}\n${data.fileData}`;
        }, '');

        const tempFileName = `${TEMP_PATH}/tmpfile.md`;

        await new Promise(resolve => fs.mkdir(TEMP_PATH, resolve));

        fs.writeFileSync(tempFileName, bookText);

        const errors = [];
        const {
            metadata,
            lookAndFeel,
            txtInputOptions,
            pdfOutputOptions,
            epubOutputOptions,
            mobiOutputOptions,
            azw3OutputOptions
        } = require(`../${language}/metadata`);

        if (bookTypes.includes('pdf')) {
            let options = {
                language,
                ...metadata,
                ...lookAndFeel,
                ...txtInputOptions,
                ...pdfOutputOptions
            }
    
            const error = await convertBook(tempFileName, options, 'pdf');
            error && errors.push(error);
        }

        if (bookTypes.includes('azw3')) {
            let options = {
                language,
                ...metadata,
                ...lookAndFeel,
                ...txtInputOptions,
                ...azw3OutputOptions
            }
    
            const error = await convertBook(tempFileName, options, 'azw3');
            error && errors.push(error);
        }

        if (bookTypes.includes('epub')) {
            options = {
                language,
                ...metadata,
                ...lookAndFeel,
                ...txtInputOptions,
                ...epubOutputOptions
            }
    
            const error = await convertBook(tempFileName, options, 'epub');
            error && errors.push(error);
            if (!error) await createMobi(options);
        }

        if (errors.length) process.exit(1);
    }
})();
