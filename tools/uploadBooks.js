require('dotenv').config()
const fs = require('fs');
const commander = require('commander');
const AWS = require('aws-sdk');
const S3 = new AWS.S3({
    apiVersion: '2006-03-01',
    credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    }
});
const { version } = require('../package.json');

const docPath = `${__dirname}/tmp`;

(async () => {
    commander
        .version('1.0.0')
        .option('-b, --book [book name]', 'Specify the book name. Default: 2064')
        .option('-t, --types [epub,pdf,mobi]', 'Specify the type of book, default: epub,pdf,mobi')
        .option('-l, --languages [de]', 'Specify the languages, default: de')
        .option('-u, --build [build number]', 'Pipeline build number')
        .option('-c, --commit [git commit]', 'Commit Id')    
        .parse(process.argv);


    const bookName = commander.book || '2064';
    const bookTypes = commander.types && commander.types.split(',') || ['epub', 'mobi'];
    const languages = commander.languages && commander.languages.split(',') || ['de'];
    const build = commander.build || 'snapshot';
    const commit = commander.commit && commander.commit.substr(0, 7) || 'snapshot';

    await Promise.all(bookTypes.map(bookType => Promise.all(languages.map(language => {
            return new Promise(async (resolve, reject) => {
                const fileName = `${docPath}/${bookName}_${language}.${bookType}`;
                const Key = `public/${bookName}_${language}_${version}.${build}.${commit}.${bookType}`;

                let fileContent;
                try {
                    fileContent = fs.readFileSync(fileName);
                } catch(error) {
                    console.error(`Error occurred: ${ error.message || JSON.stringify(error) }`);
                    process.exit(1);
                }
                
                console.log(`Uploading '${fileName} ...'`);
                S3.upload({
                    ACL: 'public-read',
                    Body: fileContent,
                    Bucket: '2064',
                    Key
                }, err => {
                    if (err) reject(err);
                    else {
                        console.log(`... uploaded ${bookType} / ${language}. Address: https://2064.s3.eu-central-1.amazonaws.com/${Key}`);
                        resolve();
                    }
                });    
            });
        }))
    )).catch(error => {
        console.error(`Error occurred: ${ error.message || JSON.stringify(error) }`);
        process.exit(1);
    });
})();
